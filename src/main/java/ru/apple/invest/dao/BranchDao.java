package ru.apple.invest.dao;

import ru.apple.invest.model.Branch;
import ru.peak.ml.core.dao.CommonDao;

import javax.persistence.TypedQuery;
import java.util.List;

/**
 *
 */
public class BranchDao extends CommonDao<Branch> {

    public Branch findBranchByName(String name) {
        if (name == null) {
            return null;
        }
        TypedQuery<Branch> query = getTypedQueryWithoutSecurityCheck("select o from Branch o where o.name = :name", Branch.class);
        query.setParameter("name", name);
        List<Branch> resultList = query.getResultList();
        if (!resultList.isEmpty()) {
            return resultList.get(0);
        } else {
            return null;
        }
    }

}