package ru.apple.invest.dao;

import ru.apple.invest.model.Customer;
import ru.peak.ml.core.dao.CommonDao;

import javax.persistence.TypedQuery;
import java.util.List;

/**
 *
 */
public class CustomerDao extends CommonDao<Customer> {

    public Customer findCustomerByName(String name) {
        if (name == null) {
            return null;
        }
        TypedQuery<Customer> query = getTypedQueryWithoutSecurityCheck("select o from Customer o where o.name = :name", Customer.class);
        query.setParameter("name", name);
        List<Customer> resultList = query.getResultList();
        if (!resultList.isEmpty()) {
            return resultList.get(0);
        } else {
            return null;
        }
    }

}