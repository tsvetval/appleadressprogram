package ru.apple.invest.dao;

import ru.apple.invest.model.Works;
import ru.peak.ml.core.dao.CommonDao;

import javax.persistence.TypedQuery;
import java.util.List;

/**
 *
 */
public class WorksDao extends CommonDao<Works> {

    public Works findWorkByNumber(String number) {
        TypedQuery<Works> query = getTypedQueryWithoutSecurityCheck("select o from Works o where o.number = :number", Works.class);
        query.setParameter("number", number);
        List<Works> resultList = query.getResultList();
        if (!resultList.isEmpty()) {
            return resultList.get(0);
        } else {
            return null;
        }
    }

    public Works findWorkByName(String name) {
        return null;
    }

}
