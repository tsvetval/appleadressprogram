package ru.apple.invest.model;

import org.eclipse.persistence.internal.dynamic.DynamicPropertiesManager;
import ru.peak.ml.core.model.MlDynamicEntityImpl;

import java.util.List;

/*
* Детали
* */
public class Details extends MlDynamicEntityImpl {
    public static DynamicPropertiesManager DPM = new DynamicPropertiesManager();

    @Override
    public DynamicPropertiesManager fetchPropertiesManager() {
        return DPM;
    }

    public static final String ID = "id";
    public static final String YEAR = "year";
    public static final String PLANE_AMOUNT = "plane_amount";
    public static final String ALLOCATED_AMOUNT = "allocated_amount";
    public static final String PERCENT = "percent";
    public static final String WORK = "work";
    public static final String BRANCH = "branch";
    public static final String CUSTOMER = "customer";
    public static final String POINTS = "points";
    public static final String DISTRICT = "district";

    public Long getId() {
        return get(ID);
    }
    public Long getYear() {
        return get(YEAR);
    }
    public Double getPlaneAmount() {
        return get(PLANE_AMOUNT);
    }
    public Double getAllocatedAmount() {
        return get(ALLOCATED_AMOUNT);
    }
    public Double getPercent() {
        return get(PERCENT);
    }
    public Works getWork(){
        return get(WORK);
    }
    public Branch getBranch(){
        return get(BRANCH);
    }
    public Customer getCustomer(){
        return get(CUSTOMER);
    }
    public List<Point> getPoints(){
        return get(POINTS);
    }
    public District getDistrict(){return get(DISTRICT);}


    public void setId(Long value){
        set(ID,value);
    }
    public void setYear(Long value){
        set(YEAR,value);
    }
    public void setPlaneAmount(Double value){
        set(PLANE_AMOUNT,value);
    }
    public void setAllocatedAmount(Double value){
        set(ALLOCATED_AMOUNT,value);
    }
    public void setPercent(Double value){
        set(PERCENT,value);
    }
    public void setWork(Works value){
        set(WORK,value);
    }
    public void setBranch(Branch value){
        set(BRANCH,value);
    }
    public void setCustomer(Customer value){
        set(CUSTOMER,value);
    }
    public void setPoints(List<Point> value){
        set(POINTS,value);
    }
    public void setDistrict(District value){
        set(DISTRICT,value);
    }

}
