package ru.apple.invest.model;

import org.eclipse.persistence.internal.dynamic.DynamicPropertiesManager;
import ru.peak.ml.core.model.MlDynamicEntityImpl;

import java.util.List;

/*
* Отрасль
* */
public class District extends MlDynamicEntityImpl {
    public static DynamicPropertiesManager DPM = new DynamicPropertiesManager();

    @Override
    public DynamicPropertiesManager fetchPropertiesManager() {
        return DPM;
    }

    public static final String ID = "id";
    public static final String NAME = "name";

    public Long getId(){
        return get(ID);
    }
    public String getName(){
        return get(NAME);
    }

    public void setId(Long value){
        set(ID,value);
    }
    public void setName(String  value){
        set(NAME,value);
    }
}
