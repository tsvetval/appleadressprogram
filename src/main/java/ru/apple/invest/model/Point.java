package ru.apple.invest.model;

import org.eclipse.persistence.internal.dynamic.DynamicPropertiesManager;
import ru.peak.ml.core.model.MlDynamicEntityImpl;

import java.util.List;

public class Point extends MlDynamicEntityImpl {
    public static DynamicPropertiesManager DPM = new DynamicPropertiesManager();

    @Override
    public DynamicPropertiesManager fetchPropertiesManager() {
        return DPM;
    }

    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String DETAILS = "details";
    public static final String COORD = "coord";
    public static final String POINT_TYPE = "pointType";
    public static final String TEXT = "text";
    public static final String IMAGE_GALLERY = "image_gallery";

    public Long getId(){
        return get(ID);
    }
    public String getName(){
        return get(NAME);
    }
    public List<Details> getDetails(){
        return get(DETAILS);
    }
    public String getCoord(){
        return get(COORD);
    }
    public String getText(){
        return get(TEXT);
    }
    public List<MlDynamicEntityImpl> getImages(){return get(IMAGE_GALLERY);}

    public PointType getPointType(){
        return get(POINT_TYPE);
    }

    public void setId(Long value){
        set(ID,value);
    }
    public void setName(String  value){
        set(NAME,value);
    }
    public void setDetails(List<Details> value){
        set(DETAILS,value);
    }
    public void setPointType(PointType value){
        set(POINT_TYPE,value);
    }
    public void setCoord(String value){
        set(COORD,value);
    }


}
