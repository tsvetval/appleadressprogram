package ru.apple.invest.model;

import org.eclipse.persistence.internal.dynamic.DynamicPropertiesManager;
import ru.peak.ml.core.model.MlDynamicEntityImpl;

import java.util.List;

/*
* Работы
* */
public class Works extends MlDynamicEntityImpl {
    public static DynamicPropertiesManager DPM = new DynamicPropertiesManager();

    @Override
    public DynamicPropertiesManager fetchPropertiesManager() {
        return DPM;
    }

    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String NUMBER = "number";
    public static final String DETAILS = "details";

    public static final String LITLE_NAME = "litleName";


    public Long getId(){
        return get(ID);
    }
    public String getName(){
        return get(NAME);
    }
    public String getLitleName(){
        return get(LITLE_NAME);
    }

    public List<Details> getDetails(){
        return get(DETAILS);
    }
    public String getNumber(){
        return get(NUMBER);
    }

    public void setId(Long value){
        set(ID,value);
    }
    public void setName(String  value){
        set(NAME,value);
    }
    public void setLitleName(String  value){
        set(LITLE_NAME,value);
    }
    public void setDetails(List<Details> value){
        set(DETAILS,value);
    }

    public void setNumber(String value){
        set(NUMBER,value);
    }
}
