package ru.apple.invest.services;

import com.google.inject.Inject;
import com.google.inject.persist.Transactional;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.apple.invest.dao.BranchDao;
import ru.apple.invest.dao.CustomerDao;
import ru.apple.invest.dao.DetailDao;
import ru.apple.invest.dao.WorksDao;
import ru.apple.invest.model.Branch;
import ru.apple.invest.model.Customer;
import ru.apple.invest.model.Details;
import ru.apple.invest.model.Works;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class ImportInvestXlsService {

    @Inject
    WorksDao worksDao;
    @Inject
    CustomerDao customerDao;
    @Inject
    BranchDao branchDao;
    @Inject
    DetailDao detailDao;
    private static final Logger log = LoggerFactory.getLogger(ImportInvestXlsService.class);

    @Transactional(rollbackOn = Exception.class)
    public ImportResult doImport(Path filepath) throws Exception {
        log.debug(String.format("Start import file with path %1$s", filepath.toString()));
        InputStream file = Files.newInputStream(filepath, StandardOpenOption.READ);
        ImportResult importResult = new ImportResult();
        XSSFWorkbook workbook = new XSSFWorkbook(file);
        XSSFSheet sheet = workbook.getSheetAt(0);

        long rowCount = (long) sheet.getLastRowNum();
        Row row = sheet.getRow(0);
        Long year = Long.parseLong(getCellDataAsString(row.getCell(0)));
        String customerName = null;
        String branchName = null;
        Integer importedCount = 0;
        Integer notImportedCount = 0;
        for (int i = 1; i <= rowCount; i++) {
            log.debug(String.format("Start import Row %1$d", i));
            try {
                row = sheet.getRow(i);
                String col1 = getCellDataAsString(row.getCell(0));
                if (col1.trim().toUpperCase().startsWith("ЗАКАЗЧИК:")) {
                    customerName = col1.trim().toUpperCase().replaceFirst("ЗАКАЗЧИК:", "").trim();
                } else if (col1.trim().toUpperCase().startsWith("ОТРАСЛЬ:")) {
                    branchName = col1.trim().toUpperCase().replaceFirst("ОТРАСЛЬ:", "").trim();
                } else {
                    // Работы
                    // Ищем рабоиту по номеру
                    String workNumber = null;
                    if (col1.startsWith("(") && col1.indexOf(")") > 0) {
                        workNumber = col1.substring(1, col1.indexOf(")"));
                    }
                    Works work = null;
                    Details detail = new Details();
                    detailDao.persistWithoutSecurityCheck(detail);
                    if (workNumber != null) {
                        work = worksDao.findWorkByNumber(workNumber);
                        if (work != null && work.getDetails() != null) {
                            work.getDetails().add(detail);
                        }
                    }

                    if (work == null) {
                        // Если не нашли работу то создаем новую
                        work = new Works();
                        work.setNumber(workNumber);
                        work.setName(col1);
                        List<Details> detailsList = new ArrayList<>();
                        detailsList.add(detail);
                        work.setDetails(detailsList);
                    }

                    // Заполняем детализацию

                    // Ищем заказчика
                    Customer customer = customerDao.findCustomerByName(customerName);
                    if (customer == null) {
                        customer = new Customer();
                        customer.setName(customerName);
                        customerDao.persistWithoutSecurityCheck(customer);
                    }
                    detail.setCustomer(customer);

                    // Ищем отрасль
                    Branch branch = branchDao.findBranchByName(branchName);
                    if (branch == null) {
                        branch = new Branch();
                        branch.setName(branchName);
                        branchDao.persistWithoutSecurityCheck(branch);
                    }
                    detail.setBranch(branch);

                    try {
                        detail.setPlaneAmount(Double.parseDouble(trimDouble(getCellDataAsString(row.getCell(1)))));
                    } catch (Exception e) {
                        detail.setPlaneAmount(0.0);
                    }
                    try {
                        detail.setAllocatedAmount(Double.parseDouble(trimDouble(getCellDataAsString(row.getCell(2)))));
                    } catch (Exception e) {
                        detail.setAllocatedAmount(0.0);
                    }
                    try {
                        detail.setPercent(Double.parseDouble(trimDouble(getCellDataAsString(row.getCell(3)))));
                    } catch (Exception e) {
                        detail.setPercent(0.0);
                    }
                    detail.setYear(year);

                    detailDao.persistWithoutSecurityCheck(detail);
                    detail.setWork(work);

                    worksDao.persistWithoutSecurityCheck(work);
                    importedCount++;
                }
            } catch (Throwable e) {
                notImportedCount++;
                importResult.addError(i, "Невозможно импортировать строку");
                log.warn(String.format("Some error while importing row : %1$d", i - 1));
                throw new Exception( "Невозможно импортировать строку" + (i - 1));
            }
        }
        importResult.setImportedCount(importedCount.toString());
        return importResult;
    }


    private String trimDouble(String in) {
        return in.replaceAll("\\s", "").replaceAll(String.valueOf((char) 160), "").replace(",", ".");
    }

    private String getCellDataAsString(Cell cell) {
        if (cell == null) {
            return "";
        }

        switch (cell.getCellType()) {
            case Cell.CELL_TYPE_BOOLEAN:
                Boolean boolVal = cell.getBooleanCellValue();
                return boolVal.toString().trim();
            case Cell.CELL_TYPE_NUMERIC:
                Double doubleVal = cell.getNumericCellValue();
                return String.valueOf(doubleVal.intValue()).trim();
            case Cell.CELL_TYPE_STRING:
                return cell.getStringCellValue().trim();
            default:
                return "";
        }
    }
}
