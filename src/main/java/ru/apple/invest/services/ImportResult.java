package ru.apple.invest.services;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 */
public class ImportResult {
    private final Map<Integer, String> resultMap = new TreeMap<>();
    private String importedCount;
    private String totlaCount;
    public void addError(Integer rowIndex, String error){
        resultMap.put(rowIndex, error);
    }

    public String getImportedCount() {
        return importedCount;
    }

    public void setImportedCount(String importedCount) {
        this.importedCount = importedCount;
    }

    public String getTotlaCount() {
        return totlaCount;
    }

    public void setTotlaCount(String totlaCount) {
        this.totlaCount = totlaCount;
    }
}
