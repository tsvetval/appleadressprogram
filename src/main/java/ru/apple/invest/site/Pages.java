package ru.apple.invest.site;

/**
 *
 */
public abstract class   Pages {
     public static final String MAIN_PAGE_URL = "/page/main";
     public static final String MAIN_PAGE_ID = "mainPage";
     public static final String MAIN_PAGE_TEMPLATE =  "main-page.hml";

     public static final String TABLE_PAGE_URL = "/page/object/list";
     public static final String TABLE_PAGE_ID = "pageObjectList";
     public static final String TABLE_PAGE_TEMPLATE = "object-list-page.hml";

     public static final String MAP_PAGE_URL = "/page/points";
     public static final String MAP_PAGE_ID = "pointsPage";
     public static final String MAP_PAGE_TEMPLATE = "points.hml";

     public static final String DETAIL_PAGE_URL = "/page/points/details";
     public static final String DETAIL_PAGE_ID = "pointsDetails";
     public static final String DETAIL_PAGE_TEMPLATE = "details.hml";

     public static final String WORKS_PAGE_URL = "/page/points/works";
     public static final String WORKS_PAGE_ID = "pointWorks";
     public static final String WORKS_PAGE_TEMPLATE = "works.hml";



     public static final String IMPORT_PAGE_URL = "/import/investitems";
     public static final String IMPORT_PAGE_ID = "ImportXlsInvestItemsPage";
}
