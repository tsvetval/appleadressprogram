package ru.apple.invest.site.external;

import ru.apple.invest.site.Pages;
import ru.ml.core.controller.MlPage;
import ru.ml.core.controller.annotation.Page;
import ru.ml.core.controller.annotation.Resource;
import ru.peak.ml.web.common.MlHttpServletRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Главная старница сайта
 */
@Page(title = "Главня страница сайта", projectTemplate = Pages.MAIN_PAGE_TEMPLATE)
@Resource(description = "Главня страница сайта", id = Pages.MAIN_PAGE_ID, url = Pages.MAIN_PAGE_URL)
public class MainPage implements MlPage {

}
