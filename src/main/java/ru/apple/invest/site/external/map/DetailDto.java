package ru.apple.invest.site.external.map;

import ru.apple.invest.model.Branch;
import ru.apple.invest.model.Customer;
import ru.apple.invest.model.Details;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by d_litovchenko on 30.11.2015.
 */
public class DetailDto {
    Customer customer;
    Branch branch;
    List<Details> details = new ArrayList<>();

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }

    public List<Details> getDetails() {
        return details;
    }
}
