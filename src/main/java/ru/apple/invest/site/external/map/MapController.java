package ru.apple.invest.site.external.map;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.inject.Inject;
import ru.apple.invest.model.Details;
import ru.apple.invest.model.District;
import ru.apple.invest.model.Point;
import ru.apple.invest.model.PointType;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.ml.core.common.exceptions.MlServerException;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.ml.core.controller.MlController;
import ru.ml.core.controller.annotation.Controller;
import ru.ml.core.controller.annotation.Resource;
import ru.ml.core.controller.annotation.ResourceContext;
import ru.peak.ml.core.dao.CommonDao;
import ru.peak.ml.core.filter.FilterBuilder;
import ru.peak.ml.core.filter.result.FilterResult;
import ru.peak.ml.web.common.MlHttpServletRequest;
import ru.peak.ml.web.common.MlHttpServletResponse;
import ru.peak.ml.web.service.annotations.PageBlockAction;

import java.util.ArrayList;
import java.util.List;

@Resource(description = "Контроллер отображения точек на карте", id = "pointContoller", url = "/points")
@Controller
public class MapController implements MlController {
    @Inject
    CommonDao commonDao;

    @Override
    public void serve(MlHttpServletRequest mlHttpServletRequest, MlHttpServletResponse mlHttpServletResponse, ResourceContext resourceContext) throws MlApplicationException, MlServerException {

    }

   /* @PageBlockAction(action = "getPoints", dataType = MlHttpServletResponse.DataType.JSON)
    public void getPoints(MlHttpServletRequest param, MlHttpServletResponse resp, ResourceContext resourceContext) throws MlApplicationException, MlServerException {
        List<Point> points;
        if (param.hasLong("year")) {
            Long year = param.getLong("year");
            points = commonDao.getQueryWithoutSecurityCheck("select o from Point o left join o.details d where d.year = :year").setParameter("year", year).getResultList();
        } else {
            points = commonDao.getQueryWithoutSecurityCheck("select o from Point o ").getResultList();
        }
        JsonArray jsonArray = new JsonArray();
        for (Point point : points) {
            JsonObject jsonObject = new JsonObject();
            jsonObject.add("coord", new JsonPrimitive(point.getCoord()));
            jsonObject.add("id", new JsonPrimitive(point.getId()));
            jsonObject.add("title", new JsonPrimitive(point.getName() == null ? "" : point.getName()));
            jsonArray.add(jsonObject);
        }
        resp.addDataToJson("points", jsonArray);
    }*/

    @PageBlockAction(action = "getPointInfo", dataType = MlHttpServletResponse.DataType.JSON)
    public void getPointInfo(MlHttpServletRequest param, MlHttpServletResponse resp, ResourceContext resourceContext) throws MlApplicationException, MlServerException {
        if (param.hasLong("pointId")) {
            Long pointId = param.getLong("pointId");
            Point point = (Point) commonDao.findById(pointId, Point.class);
            JsonObject pointInfo = new JsonObject();
            pointInfo.add("header", new JsonPrimitive(getVal(point.getName())));
            pointInfo.add("description", new JsonPrimitive(""));
            List<DetailDto> detailDtos = new ArrayList<>();

            for (Details detail : point.getDetails()) {
                DetailDto detailDto = findDtoInList(detail, detailDtos);
                detailDto.getDetails().add(detail);
            }
            JsonArray jsonArray = new JsonArray();
            Integer id = 1;
            for (DetailDto detailDto : detailDtos) {
                JsonObject cust = new JsonObject();
                cust.add("id", new JsonPrimitive(id++));
                if (detailDto.getCustomer() != null) {
                    cust.add("custName", new JsonPrimitive(getVal(detailDto.getCustomer().getName())));
                }
                if (detailDto.getBranch() != null) {
                    cust.add("branchName", new JsonPrimitive(getVal(detailDto.getBranch().getName())));
                }
                JsonArray years = new JsonArray();
                JsonArray allAmount = new JsonArray();
                JsonArray doneAmount = new JsonArray();
                String descr = "";
                Long workId = null;
                for (Details details : detailDto.getDetails()) {
                    years.add(new JsonPrimitive(details.getYear()));
                    allAmount.add(new JsonPrimitive(details.getAllocatedAmount()));
                    doneAmount.add(new JsonPrimitive(Math.round((details.getAllocatedAmount() / 100) * details.getPercent())));
                    descr += details.getWork().getLitleName();
                    workId = details.getWork().getId();
                }
                cust.add("worksName", new JsonPrimitive(descr));
                cust.add("workId", new JsonPrimitive(workId));
                cust.add("allAmount", allAmount);
                cust.add("doneAmount", doneAmount);
                cust.add("years", years);
                jsonArray.add(cust);
            }
            ;
            pointInfo.add("customers", jsonArray);
            pointInfo.add("id", new JsonPrimitive(pointId));
            resp.addDataToJson("pointInfo", pointInfo);
        }
    }

    @PageBlockAction(action = "getFilterParameters", dataType = MlHttpServletResponse.DataType.JSON)
    public void getFilterParameters(MlHttpServletRequest params, MlHttpServletResponse resp, ResourceContext resourceContext) throws MlApplicationException, MlServerException {
        List<Long> yars = commonDao.getQueryWithoutSecurityCheck("select distinct o.year from Details o order by o.year desc").getResultList();
        JsonArray yearList = new JsonArray();
        for (Long year : yars) {
            yearList.add(new JsonPrimitive(year));
        }
        resp.addDataToJson("years", yearList);

        List<PointType> pointTypes = commonDao.getQueryWithoutSecurityCheck("select distinct o from PointType o order by o.id").getResultList();
        JsonArray pointTypeList = new JsonArray();
        for (PointType pointType : pointTypes) {
            JsonObject tmpObject = new JsonObject();
            tmpObject.add("id", new JsonPrimitive(pointType.getId()));
            tmpObject.add("text", new JsonPrimitive(pointType.getName()));
            pointTypeList.add(tmpObject);
        }
        resp.addDataToJson("pointTypes", pointTypeList);

        List<District> districts = commonDao.getQueryWithoutSecurityCheck("select distinct o from District o order by o.id").getResultList();
        JsonArray districtsList = new JsonArray();
        for (District district : districts) {
            JsonObject tmpObject = new JsonObject();
            tmpObject.add("id", new JsonPrimitive(district.getId()));
            tmpObject.add("text", new JsonPrimitive(district.getName()));
            districtsList.add(tmpObject);
        }
        resp.addDataToJson("districts", districtsList);
    }

    @PageBlockAction(action = "getPointList", dataType = MlHttpServletResponse.DataType.JSON)
    public void getPointList(MlHttpServletRequest params, MlHttpServletResponse resp, ResourceContext resourceContext) throws MlApplicationException, MlServerException {

        FilterBuilder filterBuilder = GuiceConfigSingleton.inject(FilterBuilder.class);
        filterBuilder.setQueryMlClass("Point");
        filterBuilder.addOrder(params.getString("orderAttr", null), params.getString("orderType", null));
        filterBuilder.setPageNumber(params.getLong("currentPage", 1L));
        filterBuilder.setRowPerPage(params.getLong("objectsPerPage", 10L));
        filterBuilder.useExtendedSearch();

        String jsonSettings = params.getString("filterValues", "[]");
        Gson gson = new Gson();
        JsonArray filterArray = gson.fromJson(jsonSettings, JsonArray.class);
        for (int i = 0; i < filterArray.size(); i++) {
            JsonObject object = (JsonObject) filterArray.get(i);
            String attrName = object.get("entityName").getAsString();
            while (object.has("children")) {
                object = (JsonObject) object.get("children").getAsJsonArray().get(0);
                attrName += "." + object.get("entityName").getAsString();
            }
            if (object.get("op") != null && !object.get("op").isJsonNull()) {
                filterBuilder.addExtendedParameter(attrName, object.get("op").getAsString(), object.get("value").getAsString());
            }
        }


        if (filterBuilder.getOrderMap() == null || filterBuilder.getOrderMap().isEmpty()) {
            filterBuilder.addOrder("id", "desc");

        }
        FilterResult filterResult = filterBuilder.buildFilterResult();
        List<Point> objectList = (List<Point>) (List<?>) filterResult.getResultList();

        JsonArray jsonArray = new JsonArray();
        for (Point point : objectList) {
            JsonObject jsonObject = new JsonObject();
            jsonObject.add("id", new JsonPrimitive(point.getId()));
            jsonObject.add("name", new JsonPrimitive(point.getName()));
            String years = "";
            Double planeAmount = 0.0;
            Double allocatedAmount = 0.0;
            for (Details detail : point.getDetails()) {
                years += detail.getYear() + ",";
                planeAmount += detail.getPlaneAmount();
                allocatedAmount += detail.getAllocatedAmount();
            }

            jsonObject.add("year", new JsonPrimitive(years));
            jsonObject.add("plane_amount", new JsonPrimitive(planeAmount));
            jsonObject.add("allocated_amount", new JsonPrimitive(allocatedAmount));
            //jsonObject.add("percent", new JsonPrimitive(detail.getPercent()));
            jsonArray.add(jsonObject);
        }
        //filterBuilder.setRowPerPage(100L);
        List<Point> renderPoints = (List<Point>) (List<?>) filterBuilder.buildFilterResult().getAllRows();


        JsonArray jsonRenderPoints = new JsonArray();
        for (Point point : renderPoints) {
            JsonObject jsonObject = new JsonObject();
            jsonObject.add("coord", new JsonPrimitive(point.getCoord()));
            jsonObject.add("id", new JsonPrimitive(point.getId()));
            jsonObject.add("title", new JsonPrimitive(point.getName() == null ? "" : point.getName()));
            jsonRenderPoints.add(jsonObject);
        }
        resp.addDataToJson("points", jsonRenderPoints);

        resp.addDataToJson("objectDataList", jsonArray);
        resp.addDataToJson("RecordsCount", new JsonPrimitive(filterResult.getRecordsCount()));
        resp.addDataToJson("PagesCount", new JsonPrimitive(filterResult.getPagesCount()));
    }

    private String getVal(Object name) {
        if (name == null) {
            return "";
        }
        return name.toString();
    }

    private DetailDto findDtoInList(Details detail, List<DetailDto> detailDtos) {
        for (DetailDto detailDto : detailDtos) {
            if (detailDto.getBranch().getId().equals(detail.getBranch().getId())
                    && detailDto.getCustomer().getId().equals(detail.getCustomer().getId())) {
                return detailDto;
            }
        }
        DetailDto detailDto = new DetailDto();
        detailDto.setBranch(detail.getBranch());
        detailDto.setCustomer(detail.getCustomer());
        detailDtos.add(detailDto);
        return detailDto;
    }
}
