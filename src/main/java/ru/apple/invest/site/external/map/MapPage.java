package ru.apple.invest.site.external.map;

import ru.apple.invest.site.Pages;
import ru.ml.core.controller.MlPage;
import ru.ml.core.controller.annotation.Page;
import ru.ml.core.controller.annotation.Resource;

/**
 * Страница с картой
 */
@Page(title = "Отображение точек",projectTemplate = Pages.MAP_PAGE_TEMPLATE)
@Resource(description = "Страница отображения точек",id =  Pages.MAP_PAGE_ID,url = Pages.MAP_PAGE_URL)
public class MapPage implements MlPage {
}
