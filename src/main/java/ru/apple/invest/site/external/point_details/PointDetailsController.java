package ru.apple.invest.site.external.point_details;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.inject.Inject;
import ru.apple.invest.model.Details;
import ru.apple.invest.model.Point;
import ru.apple.invest.model.Works;
import ru.apple.invest.site.external.map.DetailDto;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.ml.core.common.exceptions.MlServerException;
import ru.ml.core.controller.MlController;
import ru.ml.core.controller.annotation.Controller;
import ru.ml.core.controller.annotation.Resource;
import ru.ml.core.controller.annotation.ResourceContext;
import ru.peak.ml.core.dao.CommonDao;
import ru.peak.ml.web.common.MlHttpServletRequest;
import ru.peak.ml.web.common.MlHttpServletResponse;
import ru.peak.ml.web.service.annotations.PageBlockAction;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
@Resource(description = "Контроллер детализации объекта", id = "objectDetailController", url = "/object/details")
@Controller
public class PointDetailsController implements MlController {
    @Inject
    CommonDao commonDao;

    @Override
    public void serve(MlHttpServletRequest mlHttpServletRequest, MlHttpServletResponse mlHttpServletResponse, ResourceContext resourceContext) throws MlApplicationException, MlServerException {

    }

    @PageBlockAction(action = "getWorkInfo", dataType = MlHttpServletResponse.DataType.JSON)
    public void getWorkInfo(MlHttpServletRequest param, MlHttpServletResponse resp, ResourceContext resourceContext) throws MlApplicationException, MlServerException {
        Long workId = param.getLong("workId");
        Works work = (Works) commonDao.findById(workId, Works.class);
        JsonObject jsonWork = serializeWork(work);
        resp.addDataToJson("work", jsonWork);
    }


    private JsonObject serializeWork(Works work) {
        JsonObject jsonWork = new JsonObject();
        jsonWork.add("name", new JsonPrimitive(work.getName()));
        List<DetailDto> detailDtos = new ArrayList<>();
        for (Details detail : work.getDetails()) {
            DetailDto detailDto = findDtoInList(detail, detailDtos);
            detailDto.getDetails().add(detail);
        }
        JsonArray points = new JsonArray();
        JsonArray custs = new JsonArray();
        Integer id = 1;
        for (DetailDto detailDto : detailDtos) {
            JsonObject cust = new JsonObject();
            cust.add("id", new JsonPrimitive(id++));
            cust.add("custName", new JsonPrimitive(getVal(detailDto.getCustomer()!= null ? detailDto.getCustomer().getName() : "")));
            cust.add("branchName", new JsonPrimitive(getVal(detailDto.getBranch() != null ? detailDto.getBranch().getName() : "")));
            JsonArray years = new JsonArray();
            detailDto.getDetails().stream().sorted((d1,d2)->Long.compare(d1.getYear(), d2.getYear())).forEach( detail ->{
            //for (Details detail : detailDto.getDetails()) {
                JsonObject year = new JsonObject();
                year.add("year", new JsonPrimitive(detail.getYear()));
                year.add("moneyAllocated", new JsonPrimitive(detail.getAllocatedAmount()));
                year.add("moneyUsed", new JsonPrimitive(Math.round((detail.getAllocatedAmount() / 100) * detail.getPercent())));
                year.add("percent", new JsonPrimitive(detail.getPercent()));
                years.add(year);
                for (Point point : detail.getPoints()) {

                        JsonObject jsonObject = new JsonObject();
                        jsonObject.add("coord", new JsonPrimitive(point.getCoord()));
                        jsonObject.add("id", new JsonPrimitive(point.getId()));
                        jsonObject.add("title", new JsonPrimitive(point.getName() == null ? "" : point.getName()));
                    if(!points.contains(jsonObject)) {
                        points.add(jsonObject);
                    }

                }
            });
            cust.add("years", years);

            JsonArray allYears = new JsonArray();
            JsonArray allAmount = new JsonArray();
            detailDto.getDetails().stream().sorted((d1,d2)->Long.compare(d1.getYear(), d2.getYear())).forEach( detail ->{
                allYears.add(new JsonPrimitive(detail.getYear()));
                allAmount.add(new JsonPrimitive(detail.getAllocatedAmount()));
            });
            cust.add("allAmount", allAmount);
            cust.add("allYears", allYears);
            custs.add(cust);
        }


        jsonWork.add("points", points);

        jsonWork.add("custs", custs);
        return jsonWork;
    }

    private DetailDto findDtoInList(Details detail, List<DetailDto> detailDtos) {
        for (DetailDto detailDto : detailDtos) {
            if (detailDto.getBranch().getId().equals(detail.getBranch().getId()) && detailDto.getCustomer().getId().equals(detail.getCustomer().getId())) {
                return detailDto;
            }
        }
        DetailDto detailDto = new DetailDto();
        detailDto.setBranch(detail.getBranch());
        detailDto.setCustomer(detail.getCustomer());

        detailDtos.add(detailDto);
        return detailDto;
    }

    private String getVal(Object name) {
        if (name == null) {
            return "";
        }
        return name.toString();
    }
}