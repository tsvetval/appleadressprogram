package ru.apple.invest.site.external.point_works;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.inject.Inject;
import org.eclipse.persistence.config.QueryHints;
import org.eclipse.persistence.config.ResultType;
import ru.apple.invest.model.Details;
import ru.apple.invest.model.Point;
import ru.apple.invest.model.Works;
import ru.apple.invest.site.external.map.DetailDto;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.ml.core.common.exceptions.MlServerException;
import ru.ml.core.controller.MlController;
import ru.ml.core.controller.annotation.Controller;
import ru.ml.core.controller.annotation.Resource;
import ru.ml.core.controller.annotation.ResourceContext;
import ru.peak.ml.core.dao.CommonDao;
import ru.peak.ml.core.model.MlDynamicEntityImpl;
import ru.peak.ml.web.common.MlHttpServletRequest;
import ru.peak.ml.web.common.MlHttpServletResponse;
import ru.peak.ml.web.helper.DownloadHelper;
import ru.peak.ml.web.service.annotations.PageBlockAction;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.LongSummaryStatistics;
import java.util.Map;

/**
 * Created by admin on 28.03.2016.
 */
@Resource(description = "Контроллер информации по точке", id = "pointDetailController", url = "/point/details")
@Controller
public class PointWorksController implements MlController {
    @Inject
    CommonDao commonDao;

    @Override
    public void serve(MlHttpServletRequest mlHttpServletRequest, MlHttpServletResponse mlHttpServletResponse, ResourceContext resourceContext) throws MlApplicationException, MlServerException {

    }

    @PageBlockAction(action = "getPointInfo")
    public void getPointInfo(MlHttpServletRequest param, MlHttpServletResponse resp, ResourceContext resourceContext) throws MlApplicationException, MlServerException, IOException {
        Long pointId = param.getLong("pointId");
        Point point = (Point) commonDao.findById(pointId, Point.class);
        JsonObject jsonPoint = serializePoint(point,param);
        resp.addDataToJson("point", jsonPoint);
    }

    private JsonObject serializePoint(Point point, MlHttpServletRequest request) throws IOException {
        JsonObject jsonPoint = new JsonObject();

        jsonPoint.add("coord", new JsonPrimitive(point.getCoord()));
        jsonPoint.add("id", new JsonPrimitive(point.getId()));
        jsonPoint.add("title", new JsonPrimitive(point.getName() == null ? "" : point.getName()));
        jsonPoint.add("text", new JsonPrimitive(point.getText() == null ? "" : point.getText()));

        jsonPoint.add("years",serializeYears(point));

        jsonPoint.add("images",serializeImages(point,request));

        return jsonPoint;
    }

    private JsonElement serializeImages(Point point, MlHttpServletRequest request) throws IOException {
        JsonArray jsonArray =new JsonArray();
        for (MlDynamicEntityImpl entity : point.getImages()) {
            String url = DownloadHelper.generateDownloadLink((byte[]) entity.get("image"), (String) entity.get("id").toString() + entity.get("image_filename"), request.getRequest().getServletContext());
            jsonArray.add(new JsonPrimitive(url));
        }
        return jsonArray;
    }

    private JsonArray serializeYears(Point point) {
        List<Map<String,Object>> years = commonDao.getQueryWithoutSecurityCheck("select d.year, sum(d.allocated_amount) aloc_sum, sum((d.allocated_amount/100)*d.percent) used_sum " +
                "from Point p join p.details d where p.id = :id group by d.year order by d.year").setParameter("id",point.getId()).setHint(QueryHints.RESULT_TYPE, ResultType.Map).getResultList();
        JsonArray resultYears = new JsonArray();
        for (Map<String, Object> year : years) {
            JsonObject jsonYear = new JsonObject();
            jsonYear.add("year",new JsonPrimitive(year.get("year").toString()));
            jsonYear.add("moneyAllocated",new JsonPrimitive(year.get("aloc_sum").toString()));
            jsonYear.add("moneyUsed",new JsonPrimitive(year.get("used_sum").toString()));
            Double aSum = (Double) year.get("aloc_sum");
            Double uSum = (Double) year.get("used_sum");
            jsonYear.add("percent",new JsonPrimitive(!Double.isNaN((uSum*100)/aSum) ? (uSum*100)/aSum : 0));
            jsonYear.add("works",serializeWork((Long) year.get("year"),point.getId()));
            resultYears.add(jsonYear);
        }
        return resultYears;
    }

    private JsonElement serializeWork(Long year, Long pId) {
        List<Works> works = commonDao.getQueryWithoutSecurityCheck("select distinct d.work from Details d join d.points p where p.id = :id and d.year = :year").setParameter("id", pId).setParameter("year",year).getResultList();
        JsonArray jsonArray = new JsonArray();
        for (Works work : works) {
            JsonObject jsonWork = new JsonObject();
            jsonWork.add("name",new JsonPrimitive(work.getName()));
            jsonWork.add("id",new JsonPrimitive(work.getId()));
            jsonArray.add(jsonWork);
        }
        return jsonArray;
    }

    private DetailDto findDtoInList(Details detail, List<DetailDto> detailDtos) {
        for (DetailDto detailDto : detailDtos) {
            if (detailDto.getBranch().getId().equals(detail.getBranch().getId()) && detailDto.getCustomer().getId().equals(detail.getCustomer().getId())) {
                return detailDto;
            }
        }
        DetailDto detailDto = new DetailDto();
        detailDto.setBranch(detail.getBranch());
        detailDto.setCustomer(detail.getCustomer());

        detailDtos.add(detailDto);
        return detailDto;
    }

    private String getVal(Object name) {
        if (name == null) {
            return "";
        }
        return name.toString();
    }
}
