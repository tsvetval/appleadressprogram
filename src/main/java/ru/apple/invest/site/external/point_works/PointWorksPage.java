package ru.apple.invest.site.external.point_works;

import ru.apple.invest.site.Pages;
import ru.ml.core.controller.MlPage;
import ru.ml.core.controller.annotation.Page;
import ru.ml.core.controller.annotation.Resource;
import ru.peak.ml.web.common.MlHttpServletRequest;

import java.util.HashMap;
import java.util.Map;

/**
 *
 */

@Page(title = "Работы объекта",projectTemplate = Pages.WORKS_PAGE_TEMPLATE)
@Resource(description = "Страница Работы объекта",id = Pages.WORKS_PAGE_ID,url = Pages.WORKS_PAGE_URL)
public class PointWorksPage implements MlPage {

    @Override
    public Map<String, Object> getBootTemplateData(MlHttpServletRequest req) {
        HashMap result = new HashMap();
        result.put("CONTEXT_PATH", req.getRequest().getContextPath());
        result.put("id",req.getLong("id"));
        return result;
    }

}
