package ru.apple.invest.site.external.table;

import ru.apple.invest.site.Pages;
import ru.ml.core.controller.MlPage;
import ru.ml.core.controller.annotation.Page;
import ru.ml.core.controller.annotation.Resource;
import ru.peak.ml.web.common.MlHttpServletRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Страница с таблицей
 */

@Page(title = "Список объектов", projectTemplate = Pages.TABLE_PAGE_TEMPLATE)
@Resource(description = "Страница списка объектов", id = Pages.TABLE_PAGE_ID, url = Pages.TABLE_PAGE_URL)
public class TablePage implements MlPage {

    public Map<String, Object> getBootTemplateData(MlHttpServletRequest req) {
//        PageControllerMetaHolder pageHolder = GuiceConfigSingleton.inject(PageControllerMetaHolder.class);
//        pageHolder.getPages()
        HashMap result = new HashMap();
        result.put("CONTEXT_PATH", req.getRequest().getContextPath());
        return result;
    }
}
