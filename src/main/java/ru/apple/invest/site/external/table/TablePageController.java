package ru.apple.invest.site.external.table;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.inject.Inject;
import ru.apple.invest.model.*;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.ml.core.common.exceptions.MlServerException;
import ru.ml.core.common.guice.GuiceConfigSingleton;
import ru.ml.core.controller.MlController;
import ru.ml.core.controller.annotation.Controller;
import ru.ml.core.controller.annotation.Resource;
import ru.ml.core.controller.annotation.ResourceContext;
import ru.peak.ml.core.dao.CommonDao;
import ru.peak.ml.core.filter.FilterBuilder;
import ru.peak.ml.core.filter.result.FilterResult;
import ru.peak.ml.core.model.MlDynamicEntityImpl;
import ru.peak.ml.web.common.MlHttpServletRequest;
import ru.peak.ml.web.common.MlHttpServletResponse;
import ru.peak.ml.web.service.annotations.PageBlockAction;

import java.util.List;

/**
 *
 */
@Resource(description = "Контроллер списка объектов", id = "objectListController",url = "/object/list/works")
@Controller
public class TablePageController  implements MlController {
    @Inject
    CommonDao commonDao;

    @Override
    public void serve(MlHttpServletRequest mlHttpServletRequest, MlHttpServletResponse mlHttpServletResponse, ResourceContext resourceContext) throws MlApplicationException, MlServerException {
       throw new RuntimeException("Unsupported functionality");
    }

    @PageBlockAction(action = "getFilterParameters", dataType = MlHttpServletResponse.DataType.JSON)
    public void getFilterParameters(MlHttpServletRequest params, MlHttpServletResponse resp, ResourceContext resourceContext) throws MlApplicationException, MlServerException {
        List<Long> yars =  commonDao.getQueryWithoutSecurityCheck("select distinct o.year from Details o order by o.year desc").getResultList();
        JsonArray yearList = new JsonArray();
        for (Long year : yars){
            yearList.add(new JsonPrimitive(year));
        }
        resp.addDataToJson("years", yearList);

        List<Customer> customers =  commonDao.getQueryWithoutSecurityCheck("select distinct o from Customer o order by o.id").getResultList();
        JsonArray customersList = new JsonArray();
        for (Customer customer : customers){
            JsonObject tmpObject = new JsonObject();
            tmpObject.add("id", new JsonPrimitive(customer.getId()));
            tmpObject.add("text", new JsonPrimitive(customer.getName()));
            customersList.add(tmpObject);
        }
        resp.addDataToJson("customers", customersList);

        List<Branch> btanches =  commonDao.getQueryWithoutSecurityCheck("select distinct o from Branch o order by o.id").getResultList();
        JsonArray btanchesList = new JsonArray();
        for (Branch branch : btanches){
            JsonObject tmpObject = new JsonObject();
            tmpObject.add("id", new JsonPrimitive(branch.getId()));
            tmpObject.add("text", new JsonPrimitive(branch.getName()));
            btanchesList.add(tmpObject);
        }
        resp.addDataToJson("branches", btanchesList);
        List<District> districts =  commonDao.getQueryWithoutSecurityCheck("select distinct o from District o order by o.id").getResultList();
        JsonArray districtsList = new JsonArray();
        for (District district : districts){
            JsonObject tmpObject = new JsonObject();
            tmpObject.add("id", new JsonPrimitive(district.getId()));
            tmpObject.add("text", new JsonPrimitive(district.getName()));
            districtsList.add(tmpObject);
        }
        resp.addDataToJson("districts", districtsList);
    }

    /**
     * Контроллер для получения списка объектоа на странице таблицы объектов
     * @param params
     * @param resp
     * @param resourceContext
     * @throws MlApplicationException
     * @throws MlServerException
     */
    @PageBlockAction(action = "getWorkList", dataType = MlHttpServletResponse.DataType.JSON)
    public void getWorkList(MlHttpServletRequest params, MlHttpServletResponse resp, ResourceContext resourceContext) throws MlApplicationException, MlServerException {

        FilterBuilder filterBuilder = GuiceConfigSingleton.inject(FilterBuilder.class);
        filterBuilder.setQueryMlClass("Details");
        filterBuilder.addOrder(params.getString("orderAttr", null), params.getString("orderType", null));
        filterBuilder.setPageNumber(params.getLong("currentPage", 1L));
        filterBuilder.setRowPerPage(params.getLong("objectsPerPage", 10L));
        filterBuilder.useExtendedSearch();

        String jsonSettings = params.getString("filterValues", "[]");
        Gson gson = new Gson();
        JsonArray filterArray = gson.fromJson(jsonSettings, JsonArray.class);
        for (int i = 0; i < filterArray.size(); i++) {
            JsonObject object = (JsonObject) filterArray.get(i);
            String attrName = object.get("entityName").getAsString();
            while (object.has("children")) {
                object = (JsonObject) object.get("children").getAsJsonArray().get(0);
                attrName += "." + object.get("entityName").getAsString();
            }
            if (object.get("op") != null && !object.get("op").isJsonNull()) {
                filterBuilder.addExtendedParameter(attrName, object.get("op").getAsString(), object.get("value").getAsString());
            }
        }


        if (filterBuilder.getOrderMap() == null || filterBuilder.getOrderMap().isEmpty()){
            filterBuilder.addOrder("id", "desc");

        }
        FilterResult filterResult = filterBuilder.buildFilterResult();
        List<Details> objectList = (List<Details>)(List<?>) filterResult.getResultList();

        JsonArray jsonArray = new JsonArray();
        for (Details detail : objectList) {
            if (detail.getWork() == null){
                continue;
            }
            JsonObject jsonObject = new JsonObject();
            jsonObject.add("id", new JsonPrimitive(detail.getWork().getId()));
            if (detail.getWork().getLitleName() != null && !detail.getWork().getLitleName().trim().equals("")){
                jsonObject.add("litleName", new JsonPrimitive(detail.getWork().getLitleName()));
            } else {
                jsonObject.add("litleName", new JsonPrimitive(detail.getWork().getName()));
            }
            jsonObject.add("year", new JsonPrimitive(detail.getYear()));
            jsonObject.add("plane_amount", new JsonPrimitive(detail.getPlaneAmount()));
            jsonObject.add("allocated_amount", new JsonPrimitive(detail.getAllocatedAmount()));
            jsonObject.add("percent", new JsonPrimitive(detail.getPercent()));
            jsonArray.add(jsonObject);
        }
        resp.addDataToJson("objectDataList", jsonArray);
        resp.addDataToJson("RecordsCount", new JsonPrimitive(filterResult.getRecordsCount()));
        resp.addDataToJson("PagesCount", new JsonPrimitive(filterResult.getPagesCount()));
    }

/*
    @PageBlockAction(action = "getPoints",dataType = MlHttpServletResponse.DataType.JSON)
    public void getPoints(MlHttpServletRequest param, MlHttpServletResponse resp, ResourceContext resourceContext) throws MlApplicationException, MlServerException {

    }*/
}
