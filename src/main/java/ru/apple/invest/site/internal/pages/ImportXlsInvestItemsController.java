package ru.apple.invest.site.internal.pages;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonPrimitive;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.apple.invest.services.ImportInvestXlsService;
import ru.apple.invest.services.ImportResult;
import ru.ml.core.common.exceptions.MlApplicationException;
import ru.ml.core.common.exceptions.MlServerException;
import ru.ml.core.controller.MlController;
import ru.ml.core.controller.annotation.Controller;
import ru.ml.core.controller.annotation.Resource;
import ru.ml.core.controller.annotation.ResourceContext;
import ru.peak.ml.core.model.page.MlPageBlockBase;
import ru.peak.ml.core.model.security.MlUser;
import ru.peak.ml.core.services.AccessService;
import ru.peak.ml.prop.Property;
import ru.peak.ml.web.common.MlHttpServletRequest;
import ru.peak.ml.web.common.MlHttpServletResponse;
import ru.peak.ml.web.service.annotations.PageBlockAction;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 *
 */
@Resource(id = "importInvestItemController", url = "/import/investitems/doimport", description = "Import Controller")
@Controller
public class ImportXlsInvestItemsController implements MlController {
    private static final Logger log = LoggerFactory.getLogger(ImportXlsInvestItemsController.class);

    @Inject
    ImportInvestXlsService importInvestXlsService;
    @Inject
    AccessService accessService;

    @PageBlockAction(action = "doImport", dataType = MlHttpServletResponse.DataType.JSON)
    public void doImport(MlHttpServletRequest params, MlHttpServletResponse resp,  ResourceContext context) {

        log.debug("StartImportFile");
        MlUser user = accessService.getCurrentUser();
        String filename = params.getString("filename");
        final Path tempPath = Paths.get(String.format("%s/user_%d/%s", Property.getTempDir(),  user.getId(), filename));
        ImportResult result = null;
        try {
            result = importInvestXlsService.doImport(tempPath);
        } catch (Exception e) {
            log.error(String.format("Error while importing file %1$s", filename));
        }

        Gson gson = new GsonBuilder().create();
        String json = gson.toJson(result);
        resp.addDataToJson("result", new JsonPrimitive(json));
    }

    @Override
    public void serve(MlHttpServletRequest mlHttpServletRequest, MlHttpServletResponse mlHttpServletResponse, ResourceContext resourceContext) throws MlApplicationException, MlServerException {
        throw new RuntimeException("Not implemented");
    }
}
