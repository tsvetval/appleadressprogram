package ru.apple.invest.site.internal.pages;

import ru.apple.invest.site.Pages;
import ru.ml.core.controller.MlPage;
import ru.ml.core.controller.annotation.Page;
import ru.ml.core.controller.annotation.PageBlock;
import ru.ml.core.controller.annotation.Resource;

/**
 *
 */
@Resource(
        id = Pages.IMPORT_PAGE_ID,
        url = Pages.IMPORT_PAGE_URL,
        description = "Страница импорта объектов инвест программы из XLS")
@Page(
        title = "Страница импорта объектов инвест программы из XLS",
        template = "layouts/center.hml",
        pageBlocks = {
                @PageBlock(
                        bootJS = "apple/blocks/import_invest_block/importInvestBoot",
                        description = "Блок импорта объектов",
                        id = "importInvestItemsPB",
                        zone = "CENTER"
                )
        }
)
public class ImportXlsInvestItemsPage implements MlPage {
}
