<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Умная сила - Городской бюджет</title>

    <!-- Bootstrap Core CSS -->
    <link type="text/css" href="${CONTEXT_PATH}/public/external/css/bootstrap.css" rel="stylesheet">

    <!-- Cubeportfolio Script Core CSS -->
    <link type="text/css" href="${CONTEXT_PATH}/public/external/js/lib/cubeportfolio/css/cubeportfolio.css" rel="stylesheet">

    <!-- Fonts -->
    <link type="text/css" href="${CONTEXT_PATH}/public/external/font-awesome/css/font-awesome.min.css" rel="stylesheet">


    <!-- Custom Theme CSS -->
    <link type="text/css" href="${CONTEXT_PATH}/public/external/css/base.css" rel="stylesheet">
    <link type="text/css" href="${CONTEXT_PATH}/public/external/css/main.css" rel="stylesheet">

    <!-- Advanced CSS -->
    <link type="text/css" href="${CONTEXT_PATH}/public/external/css/animate.css" rel="stylesheet">


</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-custom">

<!-- Navigation -->
<nav id="navbar" class="navbar navbar-custom navbar-fixed-top wp1" role="navigation">
    <div class="container">
        <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand" href="#page-top">
                Городской <span class="font-light"> бюджет</span>
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
            <ul class="nav navbar-nav">
                <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                <li class="hidden">
                    <a href="#page-top"></a>
                </li>
                <li class="page-scroll">
                    <a href="#about">В чем проблема?</a>
                </li>
                <li class="page-scroll">
                    <a href="#news">Мнение</a>
                </li>
                <li class="page-scroll">
                    <a href="#contact">Ваше участие</a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>


<!-- Intro Section -->
<section id="intro">
    <div class="mainslide-content">
        <div class="mainslide-image wp1 delay-05s">
            <img src="${CONTEXT_PATH}/public/external/img/bg-img.jpg" alt="">
        </div>

        <div class="overlay">
            <div class="container-wrapper">

                <div class="intro-info-wrapper">

                    <div class="container wp1 delay-1s">
                        <h1 class="text-center font-light">
                            Из чего состоит <span class="text-color"> бюджет</span><span class="font-bold font-nowrap"> Санкт-Петербурга</span>?
                        </h1>
                    </div>
                    <div id="cbp-mainslide" class="cbp cbp-btn1 wp1 delay-1s">
                        <div class="cbp-item slide1">
                            <div class="container">
                                <div class="cbp-item-wrapper">
                                    <div class="cbp-mainslide-body">

                                        <img class="img-responsive" src="${CONTEXT_PATH}/public/external/img/mainSlide.png" alt="">
                                        <button data-diagram="1" type="button" class="btn btn-default" data-toggle="modal" data-target="#diargamModal" data-title="Доходы бюджета Санкт-Петербурга">
                                            Подробнее о доходах бюджета
                                        </button>
                                        <button data-diagram="2" type="button" class="btn btn-default" data-toggle="modal" data-target="#diargamModal" data-title="Расходы бюджета Санкт-Петербурга">
                                            Подробнее о расходах бюджета
                                        </button>
                                    </div>
                                    <div class="cbp-mainslide-footer">
                                        Как вы смогли убедиться, почти <span id="city-money-persent" class="font-similar text-color font-bold">50%</span> доходов городского бюджета составляют наши с вами налоговые отчисления.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="cbp-item slide2">
                            <div class="container">
                                <div class="cbp-item-wrapper">
                                    <div class="cbp-mainslide-body">
                                        <span class="font-similar font-bold"> Очевидный факт:</span><br/> уровень жизни в городе напрямую зависит от прозрачности бюджета, контроля его исполнения, но также и очевидно, что никому так не важны вовремя построенные школа, детский сад или спортивная площадка в районе, как проживающим в нем горожанам!<br/>
                                        Получается, что<br/> <span class="font-similar text-color font-bold">благоустройство города финансируется из нашего с вами кошелька.</span><br/>
                                        От того, насколько грамотно будут распределены эти деньги, зависит качество жизни всего города и конкретно вашего района.
                                    </div>
                                    <div class="cbp-mainslide-footer">
                                        Ниже Вы сможете ознакомиться, на что уходят Ваши деньги и на сколько эффективно город их тратит.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div><!-- /.intro-info-wrapper -->

            </div><!-- /.container-wrapper -->
        </div><!-- /.overlay -->
    </div><!-- /.mainslide-content -->
</section><!-- /#intro -->



<!-- About Section -->
<section id="about">

    <div id="more" class="more page-scroll">
        <a href="#about">
				<span>
					<i class="fa fa-angle-down"></i>
				</span>
        </a>
    </div>

    <div class="container">

        <h1 class="section-title wp2 delay-1s"><span class="font-bold">Народный контроль</span> за нашим бюджетом</h1>
        <div class="row">
            <div class="col-lg-5 col-md-6 wp2a delay-1s">
                <img class="img-responsive" src="${CONTEXT_PATH}/public/external/img/map.jpg" alt="">
            </div><!-- /.col-lg-6 -->
            <div class="col-lg-7 col-md-6 wp2b delay-05s">
                <h2 class="sub-title">Что не так с нашим <span class="text-color">бюджетом</span>?</h2>
                <p>Благоустройство города финансируется из нашего с Вами кошелька, а от того, насколько грамотно будут распределены эти деньги, зависит качество жизни всего города и конкретно вашего района. Именно поэтому давно пора перестать стесняться и начать контролировать, куда и на что уходят наши с Вами налоги, а проект «Народный контроль» поможет в этом.</p>
                <h4>Например:</h4>
                <TABLE class="text-dark">
                    <TR><TD>Объект </TD>    <TD>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</TD><TD> Станция скорой помощи на 5 машин</TD></TR>
                    <TR><TD>Адрес </TD>    <TD>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</TD><TD> Шувалово-Озерки, квартал 25А (напротив дома 21/20)</TD></TR>
                    <TR><TD>Утвержено </TD>  <TD>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</TD><TD>19 000 000 рублей</TD></TR>
                    <TR><TD>Использовано </TD>  <TD>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</TD><TD>1 851 800 рублей <span class="detailed-text">9,7%</span></TD></TR>
                    <TR><TD>Отрасль </TD><TD>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</TD><TD>Здравоохранение</TD></TR>

                </TABLE>
                <h4 class="sub-text">
                    Вы можете самостоятельно поискать подобные примеры неразумного использования (либо неиспользования) бюджетных средств в нашем городе. Предлагаем Вам воспользоваться поиском по нашей базе ниже.
                </h4>
                <div class="media-btns buttons page-scroll">
                    <a class="btn btn-default mapSearch-btn" href="${CONTEXT_PATH}${ru.apple.invest.site.Pages.MAP_PAGE_URL}">Смотреть карту объектов<i class="fa fa-play animated"></i></a>
                    <a class="btn btn-default listSearch-btn" href="${CONTEXT_PATH}${ru.apple.invest.site.Pages.TABLE_PAGE_URL}">Расширенный поиск<i class="fa fa-chevron-right"></i></a>
                </div><!-- /.media-btns -->
            </div><!-- /.col-lg-6 -->
        </div><!-- /.row -->

    </div><!-- /.container -->

</section><!-- /#about -->



<!-- About-Sub Section -->
<section id="about-sub">
    <div class="container">
        <div class="row">
            <div class="col-lg-7 col-md-6 wp3a delay-05s">
                <h2 class="sub-title">Неизрасходование городских средств <span class="text-color">нельзя</span> сравнивать с экономией</h2>
                <p>
                    Все городские траты планируются заранее под конкретные городские нужды: строительство зданий, ремонт дорог или финансирование социальных программ, а оставшиеся средства в бюджете говорят либо о неэффективном планировании, когда деньги тратятся без назначения, либо о невыполнении возложенных на город обязательств перед горожанами.
                </p>
                <h3>Что из этого следует?</h3>
                <ul class="feature-list" style="font-size: 16px;">
                    <li><i class="fa fa-briefcase"></i>На Улице Новоселов не будет построена общеобразовательная школа на 825 мест.</li>
                    <li><i class="fa fa-ambulance"></i>Поэтический бульвар так и не дождется станции скорой медицинской помощи.</li>
                    <li><i class="fa fa-building"></i>Жители коммуналок могут отложить в долгий ящик получение в обозримом будущем собственной квартиры.</li>
                </ul>
            </div>

            <div class="col-lg-5 col-md-6 wp3b delay-1s" id="counters">
                <ul class="counters">
                    <li>
                        <span id="counter-1">20 530 363 800</span><i class="fa fa-rub"></i>
                        <h5>Недофинансирование адресной инвестиционной программы в 2013 году</h5>
                    </li>

                    <li>
                        <span id="counter-2">19 892 868 600</span><i class="fa fa-rub"></i>
                        <h5>Недофинансирование адресной инвестиционной программы в 2014 году</h5>
                    </li>

                    <li>
                        <span id="counter-3">9 963 319 900 </span><i class="fa fa-rub"></i>
                        <h5>Недофинансирование адресной инвестиционной программы в 2015 году</h5>
                    </li>
                </ul>
            </div><!-- /.col-lg-3 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /#about-sub -->

<!-- About-Sub Section -->
<section id="news">
    <div class="section-wrapper">
        <img class="man" src="${CONTEXT_PATH}/public/external/img/grigoriy.png">
        <h3>Работа Явлинского на улучшение качества процесса бюджетного регулирования <a href="http://control.umnaya-sila.ru/news.html" target="_blank" class="readmore">(Смотреть всё)</a></h3>
        <div id="cbp-news" class="cbp section-block">
            <div class="cbp-item">
                <a href="http://control.umnaya-sila.ru/news.html#cbp=news/2015-11-14-А.html" target="_blank">
                    <div class="news-title">Практика анонимной поправки БФК позорит весь петербургский парламент</div>
                    <div class="news-date">2015/11/14</div>
                </a>
            </div>
            <div class="cbp-item">
                <a href="http://control.umnaya-sila.ru/news.html#cbp=news/2015-11-07-А.html" target="_blank">
                    <div class="news-title">«Яблоко» внесет «антикризисные» поправки к закону о бюджетном процессе в Петербурге</div>
                    <div class="news-date">2015/11/07</div>
                </a>
            </div>
            <div class="cbp-item">
                <a href="http://control.umnaya-sila.ru/news.html#cbp=news/2015-11-05-A.html" target="_blank">
                    <div class="news-title">Бездействие чиновников погубит экономику Петербурга</div>
                    <div class="news-date">2015/11/05</div>
                </a>
            </div>
            <div class="cbp-item">
                <a href="http://control.umnaya-sila.ru/news.html#cbp=news/2015-10-16-А.html" target="_blank">
                    <div class="news-title">Поправка БФК к бюджету на 2016 год раскрыта – спикер ЗакСа Петербурга Макаров</div>
                    <div class="news-date">2015/10/16</div>
                </a>
            </div>

            <div class="cbp-item">
                <a href="http://control.umnaya-sila.ru/news.html#cbp=news/2015-10-09-А.html" target="_blank">
                    <div class="news-title">Вскоре будет внесен в ЗакС Петербурга проект бюджета на 2016 год</div>
                    <div class="news-date">2015/10/09</div>
                </a>
            </div>
            <div class="cbp-item">
                <a href="http://control.umnaya-sila.ru/news.html#cbp=news/2015-07-17-А.html" target="_blank">
                    <div class="news-title">Фракция «ЯБЛОКО» в Законодательном Собрании Санкт-Петербурга проголосовала против закона об исполнении городского бюджета за 2014 год</div>
                    <div class="news-date">2015/07/17</div>
                </a>
            </div>
            <div class="cbp-item">
                <a href="http://control.umnaya-sila.ru/news.html#cbp=news/2015-07-03-А.html" target="_blank">
                    <div class="news-title">Фракция «Яблоко» не поддержала корректировку бюджета на следующий год</div>
                    <div class="news-date">2015/07/03</div>
                </a>
            </div>
            <div class="cbp-item">
                <a href="http://control.umnaya-sila.ru/news.html#cbp=news/2015-04-29-А.html" target="_blank">
                    <div class="news-title">О корректировке бюджета Петербурга на 2015 год</div>
                    <div class="news-date">2015/04/29</div>
                </a>
            </div>
        </div>
    </div>
</section>

<!-- Reviews Section -->
<section id="reviews">
    <div class="container">
        <h1 class="section-title">Мнения Г.А. Явлинского</h1>

        <div id="cbp-testimonials" class="cbp cbp-btn2 section-block">
            <div class="cbp-item">
                <div class="cbp-testimonials-image">
                    <div style="background-image: url('${CONTEXT_PATH}/public/external/img/avatars/01.png')"></div>
                </div>
                <div class="cbp-testimonials-body">
                    “Не врать и не воровать!”
                </div>
                <div class="cbp-testimonials-footer">
                    10.10.2012 выступая в программе Владимира Соловьёва «Открытие нового политического сезона» на телеканале Россия-1
                </div>
            </div>
            <div class="cbp-item">
                <div class="cbp-testimonials-image">
                    <div style="background-image: url('${CONTEXT_PATH}/public/external/img/avatars/02.png')"></div>
                </div>
                <div class="cbp-testimonials-body">
                    “Главными проблемами петербургской осени станет рост цен, спад производства, снижение реальных доходов населения - это касается и страны в целом.”
                </div>
                <div class="cbp-testimonials-footer">
                    07.11.2015 комментируя «антикризисные» поправки к закону о бюджетном процессе
                </div>
            </div>
            <div class="cbp-item">
                <div class="cbp-testimonials-image">
                    <div style="background-image: url('${CONTEXT_PATH}/public/external/img/avatars/03.png')"></div>
                </div>
                <div class="cbp-testimonials-body">
                    “Если будут, как и всегда, формально вносить проект бюджета на 2016 год, то в таких условиях эффективность расходования средств, структура бюджета и экономическая политика не будут связаны с кризисными процессами, которые сейчас происходят в Петербурге – Это не будет инструментом защиты города от кризиса.”
                </div>
                <div class="cbp-testimonials-footer">
                    07.11.2015 комментируя «антикризисные» поправки к закону о бюджетном процессе
                </div>
            </div>
            <div class="cbp-item">
                <div class="cbp-testimonials-image">
                    <div style="background-image: url('${CONTEXT_PATH}/public/external/img/avatars/04.png')"></div>
                </div>
                <div class="cbp-testimonials-body">
                    “Открытость работы правительства – это и есть борьба с коррупцией. Еще борьба с коррупцией – это равенство перед законом. Борьба с коррупцией – это независимые средства массовой информации, которые могу проводить расследования. Я уж не говорю про независимый суд, про общественный контроль, про независимого прокурора, специально назначенного для борьбы с коррупцией.”
                </div>
                <div class="cbp-testimonials-footer">
                    16.10.2015 комментируя губернаторский законопроект о создании «Комиссии по координации работы по противодействию коррупции в Санкт-Петербурге»
                </div>
            </div>
            <div class="cbp-item">
                <div class="cbp-testimonials-image">
                    <div style="background-image: url('${CONTEXT_PATH}/public/external/img/avatars/05.png')"></div>
                </div>
                <div class="cbp-testimonials-body">
                    “Мы должны дать городу такой бюджет, который в равной степени обеспечит достойную жизнь наших бюджетников и создаст условия для развития малого и среднего бизнеса - не только в целях пополнения городских доходов, но и создания реальных рабочих мест для наших горожан, в первую очередь, молодёжи.”
                </div>
                <div class="cbp-testimonials-footer">
                    выдержка из брошюры "БЮДЖЕТ САНКТ-ПЕТЕРБУРГА 2012-2013" по совершенствованию бюджетного процесса
                </div>
            </div>
            <div class="cbp-item">
                <div class="cbp-testimonials-image">
                    <div style="background-image: url('${CONTEXT_PATH}/public/external/img/avatars/06.png')"></div>
                </div>
                <div class="cbp-testimonials-body">
                    “Практика анонимной поправки Бюджетно-финансового комитета позорит весь петербургский парламент.”
                </div>
                <div class="cbp-testimonials-footer">
                    14.10.2015 выступая в Законодательном Собрании Санкт-Петербурга
                </div>
            </div>
            <div class="cbp-item">
                <div class="cbp-testimonials-image">
                    <div style="background-image: url('${CONTEXT_PATH}/public/external/img/avatars/07.png')"></div>
                </div>
                <div class="cbp-testimonials-body">
                    “Это еще не кризис, еще все впереди. И если власти, как всегда, будут вносить в бюджет формальные изменения, если будут ничего не делать и ждать 3 млрд рублей из федерального центра для поддержки, то в таких условиях бюджет не будет инструментом защиты Петербурга от экономического кризиса.”
                </div>
                <div class="cbp-testimonials-footer">
                    08.10.2015 комментируя бюджет на 2016 год в Законодательном Собрании Санкт-Петербурга
                </div>
            </div>
        </div>

    </div><!-- /.container -->
</section><!-- /#reviews -->

<!-- Gallery Section -->
<section id="gallery">
    <div class="overlay"></div>

    <div class="section-wrapper">
        <h1 class="section-title">Проекты которые вызывают вопросы</h1>
        <p class="section-subtitle">Мы выбрали наиболее показательные городские проекты, которые не были реализованы в Санкт-Петербурге из-за нехватки бюджетных средств:</p>

        <div id="cbp-objects" class="cbp cbp-btn3 section-block">

            <div class="cbp-item">
                <a href="${CONTEXT_PATH}/public/external/objects/project1.html" class="cbp-caption cbp-singlePageInline" data-title="Полигон «Красный Бор». Строительство 1-ой очереди предприятия по переработке и захоронению промышленных токсичных отходов" rel="nofollow">
                    <div class="cbp-caption-defaultWrap">
                        <img src="${CONTEXT_PATH}/public/external/objects/01.jpg" alt="">
                    </div>
                    <div class="cbp-caption-activeWrap">
                        <div class="cbp-l-caption-alignCenter">
                            <div class="cbp-l-caption-body">
                                <div class="cbp-l-caption-title">Полигон «Красный Бор»</div>
                                <div class="cbp-l-caption-desc">Строительство 1-ой очереди предприятия по переработке и захоронению промышленных токсичных отходов</div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

            <div class="cbp-item">
                <a href="${CONTEXT_PATH}/public/external/objects/project2.html" class="cbp-caption cbp-singlePageInline" data-title="Стадион «Зенит» на Крестовском острове" rel="nofollow">
                    <div class="cbp-caption-defaultWrap">
                        <img src="${CONTEXT_PATH}/public/external/objects/02.jpg" alt="">
                    </div>
                    <div class="cbp-caption-activeWrap">
                        <div class="cbp-l-caption-alignCenter">
                            <div class="cbp-l-caption-body">
                                <div class="cbp-l-caption-title">Стадион «Зенит»</div>
                                <div class="cbp-l-caption-desc">на Крестовском острове</div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

            <div class="cbp-item">
                <a href="${CONTEXT_PATH}/public/external/objects/project3.html" class="cbp-caption cbp-singlePageInline" data-title="Мариинская больница: реконструкция ценой в 3,2 миллиарда рублей" rel="nofollow">
                    <div class="cbp-caption-defaultWrap">
                        <img src="${CONTEXT_PATH}/public/external/objects/03.jpg" alt="">
                    </div>
                    <div class="cbp-caption-activeWrap">
                        <div class="cbp-l-caption-alignCenter">
                            <div class="cbp-l-caption-body">
                                <div class="cbp-l-caption-title">Мариинская больница.</div>
                                <div class="cbp-l-caption-desc">Реконструкция ценой в 3,2 миллиарда рублей</div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

            <div class="cbp-item">
                <a href="${CONTEXT_PATH}/public/external/objects/project4.html" class="cbp-caption cbp-singlePageInline" data-title="Строительство крытого детского спортивного катка с искусственным льдом" rel="nofollow">
                    <div class="cbp-caption-defaultWrap">
                        <img src="${CONTEXT_PATH}/public/external/objects/04.jpg" alt="">
                    </div>
                    <div class="cbp-caption-activeWrap">
                        <div class="cbp-l-caption-alignCenter">
                            <div class="cbp-l-caption-body">
                                <div class="cbp-l-caption-title">Ледовый каток</div>
                                <div class="cbp-l-caption-desc">Строительство крытого детского спортивного катка с искусственным льдом</div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

            <div class="cbp-item">
                <a href="${CONTEXT_PATH}/public/external/objects/project5.html" class="cbp-caption cbp-singlePageInline" data-title="В Ленинградском зоопарке возведут новую кормокухню за 41млн рублей" rel="nofollow">
                    <div class="cbp-caption-defaultWrap">
                        <img src="${CONTEXT_PATH}/public/external/objects/05.jpg" alt="">
                    </div>
                    <div class="cbp-caption-activeWrap">
                        <div class="cbp-l-caption-alignCenter">
                            <div class="cbp-l-caption-body">
                                <div class="cbp-l-caption-title">Кормокухня за 41млн</div>
                                <div class="cbp-l-caption-desc">В Ленинградском зоопарке возведут новую кормокухню за 41млн рублей</div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>

<!-- Contact Section -->
<section id="contact">
    <div class="overlay"></div>
    <div class="container text-center">
        <div class="row">
            <div class="col-lg-12 wp07">

                <ul class="social-buttons">
                    <li><a href="http://vk.com/yavlinsky_yabloko" target="_blank"><i class="fa fa-vk fa-fw"></i> <span>Вконтакте</span></a></li>
                    <li><a href="https://twitter.com/yavlinskylive" target="_blank"><i class="fa fa-twitter fa-fw"></i> <span>Twitter</span></a></li>
                    <li><a href="https://www.facebook.com/groups/186598411434744/1004621596299084/" target="_blank"><i class="fa fa-facebook fa-fw"></i> <span>Facebook</span></a></li>
                    <li><a href="https://plus.google.com/+YavlinskyYabloko/posts" target="_blank"><i class="fa fa-google-plus fa-fw"></i> <span>Google+</span></a></li>
                    <li><a href="https://www.youtube.com/user/YavlinskyYabloko" target="_blank"><i class="fa fa-youtube fa-fw"></i> <span>You Tube</span></a></li>
                </ul>

            </div><!-- /.col-lg-12 -->

        </div><!-- /.row -->
        <div class="row">

            <div class="copyright">
                © При поддержке общественного движения <a href="http://umnaya-sila.ru/"> Умная Сила</a>
                <br>
                2015
            </div>
            <div class="footer-line"></div>
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /#contact -->


</body>

<!-- Core JavaScript Files -->
<script type="text/javascript" src="${CONTEXT_PATH}/public/external/js/jquery-1.10.2.js"></script>
<script type="text/javascript" src="${CONTEXT_PATH}/public/external/js/bootstrap.min.js"></script>
<script type="text/javascript" src="${CONTEXT_PATH}/public/external/js/jquery.easing.min.js"></script>
<script type="text/javascript" src="${CONTEXT_PATH}/public/js/libs/jquery-cookie-master/src/jquery.cookie.js"></script>

<!-- JavaScript -->
<script type="text/javascript" src="${CONTEXT_PATH}/public/external/js/lib/jquery.appear.js"></script>
<script type="text/javascript" src="${CONTEXT_PATH}/public/external/js/lib/cubeportfolio/js/jquery.cubeportfolio.min.js"></script>
<script type="text/javascript" src="${CONTEXT_PATH}/public/external/js/lib/jquery.animateNumber.min.js"></script>
<script type="text/javascript" src="${CONTEXT_PATH}/public/external/js/lib/waypoints.min.js"></script>

<!--<script type="text/javascript" src="js/lib/jquery.resizeEnd.js"></script>-->

<!-- Custom Theme JavaScript -->
<script type="text/javascript" src="${CONTEXT_PATH}/public/external/js/base.js"></script>
<script type="text/javascript" src="${CONTEXT_PATH}/public/external/js/main.js"></script>

<script src="${CONTEXT_PATH}/public/external/js/lib/Chart.min.js"></script>

<!-- Modal -->
<div class="modal fade" id="diargamModal" tabindex="-1" role="dialog" aria-labelledby="diargamModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="diargamModalLabel"></h4>
            </div>
            <div class="modal-body">
                <div class="diagramm">
                    <div class="canvas-holder">
                    </div>
                </div>
                <div class="legendDiv" id="legendDiv"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    var dntData1 = [
        {
            value: 23386715100,
            color: "#2DAAE2",
            highlight: "#929daf",
            label: "Безвозмездные поступления"
        },
        {
            value: 170543639300,
            color: "#FB0000",
            highlight: "#929daf",
            label: "Налог на доходы физических лиц"
        },
        {
            value: 92774887200,
            color: "#f56262",
            highlight: "#929daf",
            label: "Налог на прибыль организаций"
        },
        {
            value: 27118304600,
            color: "#5a6170",
            highlight: "#929daf",
            label: "Налог на имущество организаций"
        },
        {
            value: 20751081900,
            color: "#5f6675",
            highlight: "#929daf",
            label: "Акцизы"
        },
        {
            value: 19561085600,
            color: "#656c7b",
            highlight: "#929daf",
            label: "Доходы от использования имущества, находящегося в государственной и муниципальной собственности"
        },
        {
            value: 12264677000,
            color:"#6b7382",
            highlight: "#929daf",
            label: "Налоги на совокупный доход"
        },
        {
            value: 9729914300,
            color: "#707988",
            highlight: "#929daf",
            label: "Доходы от продажи материальных и нематериальных активов"
        },
        {
            value: 9507936800,
            color: "#777f8f",
            highlight: "#929daf",
            label: "Транспортный налог"
        },
        {
            value: 5981640000,
            color: "#7c8595",
            highlight: "#929daf",
            label: "Земельный налог"
        },
        {
            value: 3365581800,
            color: "#818b9c",
            highlight: "#929daf",
            label: "Доходы от оказания платных услуг и компенсации затрат государства"
        },
        {
            value: 2494217400,
            color: "#8791a2",
            highlight: "#929daf",
            label: "Штрафы, санкции, возмещение ущерба"
        },
        {
            value: 1333260700,
            color: "#8c96a7",
            highlight: "#929daf",
            label: "Государственная пошлина"
        },
        {
            value: 778225600,
            color: "#909aac",
            highlight: "#929daf",
            label: "Платежи при пользовании природными ресурсами"
        },
        {
            value: 2222600,
            color: "#929daf",
            highlight: "#929daf",
            label: "Административные платежи и сборы"
        },

    ];
    var dntData2 = [
        {
            value: 106963982500,
            color: "#525865",
            highlight: "#929daf",
            label: "Образование"
        },
        {
            value: 96925744900,
            color: "#565c6a",
            highlight: "#929daf",
            label: "Национальная экономика"
        },
        {
            value: 72604896700,
            color: "#5a6170",
            highlight: "#929daf",
            label: "Здравоохранение"
        },
        {
            value: 59796713200,
            color: "#5f6675",
            highlight: "#929daf",
            label: "Социальная политика"
        },
        {
            value: 48844919300,
            color: "#656c7b",
            highlight: "#929daf",
            label: "Жилищно-коммунальное хозяйство"
        },
        {
            value: 30195611800,
            color:"#6b7382",
            highlight: "#929daf",
            label: "Общегосударственные вопросы"
        },
        {
            value: 15000612500,
            color: "#707988",
            highlight: "#929daf",
            label: "Культура и кинематография"
        },

        {
            value: 12454549200,
            color: "#777f8f",
            highlight: "#929daf",
            label: "Физическая культура и спорт"
        },
        {
            value: 3388614100,
            color: "#7c8595",
            highlight: "#929daf",
            label: "Национальная безопасность и правоохранительная деятельность"
        },
        {
            value: 1690451500,
            color: "#818b9c",
            highlight: "#929daf",
            label: "Охрана окружающей среды"
        },
        {
            value: 1405276900,
            color: "#8791a2",
            highlight: "#929daf",
            label: "Средства массовой информации"
        },
        {
            value: 968498400,
            color: "#8c96a7",
            highlight: "#929daf",
            label: "Обслуживание государственного и муниципального долга"
        },
        {
            value: 897688800,
            color: "#909aac",
            highlight: "#929daf",
            label: "Межбюджетные трансферты общего характера бюджетам субъектов РФ и муниципальных образований"
        },
        {
            value: 99531100,
            color: "#929daf",
            highlight: "#929daf",
            label: "Национальная оборона"
        },

    ];

    var summAll1 = summ(dntData1);
    var summAll2 = summ(dntData2);

    var dntOptions1 = {
        segmentShowStroke: true,
        animateRotate: true,
        animateScale: false,
        percentageInnerCutout: 80,
        tooltipFontSize: 10,
        responsive : true,
        showScale: true,
        scaleBeginAtZero: true,
        tooltipTemplate: "<%= label %>: <%= percentage(value,summAll1) %>%",
        legendTemplate : "<table class=\"<%=name.toLowerCase()%>Legend table table-striped\"><% for (var i=0; i<segments.length; i++){%><tr><td><span class=\"legend-pin\" style=\"background-color:<%=segments[i].fillColor%>\"></span></td><td style=\"text-align: left;\"><%if(segments[i].label){%><%=segments[i].label%><%}%></td><td style=\"text-align: right; white-space: nowrap;\"><span class=\"num\"><%= numFormat(segments[i].value) %></span> р.</td></tr><%}%></table>",
    };
    var dntOptions2 = {
        segmentShowStroke: true,
        animateRotate: true,
        animateScale: false,
        percentageInnerCutout: 80,
        tooltipFontSize: 10,
        responsive : true,
        showScale: true,
        scaleBeginAtZero: true,
        tooltipTemplate: "<%= label %>: <%= percentage(value,summAll2) %>%",
        legendTemplate : "<table class=\"<%=name.toLowerCase()%>Legend table table-striped\"><% for (var i=0; i<segments.length; i++){%><tr><td><span class=\"legend-pin\" style=\"background-color:<%=segments[i].fillColor%>\"></span></td><td style=\"text-align: left;\"><%if(segments[i].label){%><%=segments[i].label%><%}%></td><td style=\"text-align: right; white-space: nowrap;\"><span class=\"num\"><%= numFormat(segments[i].value) %></span> р.</td></tr><%}%></table>",
    }

    //вычисляем сумму всех чисел
    function summ(e) {
        for(var i=0, sum=0; i<e.length; i++) {
            sum+=parseInt(e[i].value)
        }
        return(sum);
    }
    //вычисляем процент для конкретного случая
    function percentage(a,b) {
        return Math.round(((100*a)/b) * Math.pow(10, 2)) / Math.pow(10, 2) ;
    }

    $('#diargamModal').on('shown.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var modal = $(this);
        var diagram = button.attr('data-diagram'),
                title = button.attr('data-title');
        //запускаем диаграмму
        switch (diagram) {
            case '1':
                var dntData = dntData1;
                var dntOptions = dntOptions1;
                var summAll = summAll1;
                break
            case '2':
                var dntData = dntData2;
                var dntOptions = dntOptions2;
                var summAll = summAll2;
                break
            default:
        }

        $('.canvas-holder').append('<canvas class="chartDiv" id="chartDiv" width="400" height="400"></canvas><div id="textDiv" class="textDiv"> Суммарно:<br><span class="num"></span><br>рублей</div>');
        var canvas = modal.find('.modal-body canvas'),
                ctx = canvas[0].getContext("2d"),
                myLineChart = new Chart(ctx).Doughnut(dntData, dntOptions);
        modal.find('.modal-title').html(title);
        modal.find('#legendDiv').html(myLineChart.generateLegend());
        modal.find('#textDiv .num').html(numFormat(summAll1));

    }).on('hide.bs.modal',function(event){
        var modal = $(this);
        //myLineChart.destroy();
        //var canvas = modal.find('.modal-body canvas');
        //canvas.attr('width','400px').attr('height','400px');
        modal.find('.modal-body .canvas-holder').empty();
        modal.find('.modal-title').empty();
        modal.find('#legendDiv').empty();
        modal.find('#textDiv .num').empty();
        // destroy modal
        $(this).data('bs.modal', null);
    });
</script>

</html>
