// jQuery to collapse the navbar on scroll (Bootstrap Affix)
jQuery(document).ready(function($) {
	$('.navbar').affix({
		offset: {
			top: $('.navbar').height()
		}
	});
});	

// bs3 tooltips
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})

// jQuery for page scrolling feature - requires jQuery Easing plugin
$(function() {
    $('.page-scroll a').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 800, 'easeInOutExpo');
        event.preventDefault();
    });
});

// Accordion (Not used now)
$('.collapse').on('shown.bs.collapse', function(){
	$(this).parent().find(".glyphicon-plus").removeClass("glyphicon-plus").addClass("glyphicon-minus");
}).on('hidden.bs.collapse', function(){
	$(this).parent().find(".glyphicon-minus").removeClass("glyphicon-minus").addClass("glyphicon-plus");
});


// форматируем суммы в рублях
function numFormat(n, d, s) { 
	if (arguments.length == 2) { s = " "; }
	if (arguments.length == 1) { s = " "; d = "."; }
	n = n.toString();
	a = n.split(d);
	x = a[0];
	y = a[1];
	z = "";
	if (typeof(x) != "undefined") {
	for (i=x.length-1;i>=0;i--)
		z += x.charAt(i);
	z = z.replace(/(\d{3})/g, "$1" + s);
	if (z.slice(-s.length) == s)
		z = z.slice(0, -s.length);
	x = "";
	for (i=z.length-1;i>=0;i--)
		x += z.charAt(i);
	if (typeof(y) != "undefined" && y.length > 0)
		x += d + y;
	}
	return x;
}
// заменяем все суммы на форматированные по классу <span class="num">
window.onload=function(){
	var nums = document.querySelectorAll('span.num');			
	for(ti=0;ti<nums.length;ti++) {;
		var formattedNumber = numFormat(nums[ti].innerHTML);
		nums[ti].innerHTML = formattedNumber;
	}
};