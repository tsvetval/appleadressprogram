/* 05 - Video Content BG Center Image
 -----------------------------------------------------------*/

$(window).load(function()
{
	centerContent();
});

$(window).resize(function()
{
	centerContent();
});

function centerContent()
{
	var container = $('.mainslide-image');
	var content = $('.mainslide-image img');
	content.css("left", (container.width()-content.width())/2);
	content.css("top", (container.height()-content.height())/2);
}


// Animate Number Plugin
jQuery(document).ready(function($) {
	"use strict";
	var comma_separator_number_step = $.animateNumber.numberStepFactories.separator(' ')
	var percent_number_step = $.animateNumber.numberStepFactories.append('%')
	/*	$('#city-money').animateNumber(
	 {
	 number: 7095217980,
	 easing: 'easeInQuad',
	 numberStep: comma_separator_number_step
	 }, 3000
	 );
	 */
	$('#city-money-persent').animateNumber(
		{
			number: 50,
			easing: 'easeInQuad',
			numberStep: percent_number_step
		}, 3000
	);

	var percent_number_step = $.animateNumber.numberStepFactories.append('%')
	$('#counters').appear(function() {
		$('#counter-1').animateNumber({ number: 20530363800, numberStep: comma_separator_number_step }, 2000 );
		$('#counter-2').animateNumber({ number: 19892868600, numberStep: comma_separator_number_step }, 2000 );
		$('#counter-3').animateNumber({ number: 9963319900, numberStep: comma_separator_number_step }, 2000 );
		$('#counter-1-a').animateNumber({ number: 77, numberStep: percent_number_step }, 1800 );
		$('#counter-2-a').animateNumber({ number: 88, numberStep: percent_number_step }, 1800 );
		$('#counter-3-a').animateNumber({ number: 99, numberStep: percent_number_step }, 1800 );

	},{accX: 0, accY: -200});

});



// initialize Main block 
jQuery(document).ready(function($) {
	"use strict";
	var MainSlideOptions = {
		layoutMode: 'slider',
		drag: true,
		auto: true,
		autoTimeout: 7000,
		autoPauseOnHover: true,
		showNavigation: true,
		showPagination: true,
		rewindNav: true,
		scrollByPage: true,
		gridAdjustment: 'responsive',
		mediaQueries: [{
			width: 1,
			cols: 1
		}],
		gapHorizontal: 0,
		gapVertical: 0,
		caption: '',
		displayType: 'default'
	};
	$('#cbp-mainslide').cubeportfolio(MainSlideOptions);



	var NewsOptions = {
		layoutMode: 'slider',
		drag: true,
		auto: false,
		autoTimeout: 5000,
		autoPauseOnHover: true,
		showNavigation: true,
		showPagination: false,
		rewindNav: false,
		scrollByPage: false,
		gridAdjustment: 'responsive',
		mediaQueries: [{
			width: 1500,
			cols: 5
		}, {
			width: 1100,
			cols: 4
		}, {
			width: 800,
			cols: 3
		}, {
			width: 480,
			cols: 2
		}, {
			width: 320,
			cols: 1
		}],
		gapHorizontal: 0,
		gapVertical: 0,
		caption: '',
		displayType: 'default',
		displayTypeSpeed: 100
	};
	$('#cbp-news').cubeportfolio(NewsOptions);



	var TestimonialsOptions = {
		layoutMode: 'slider',
		drag: true,
		auto: true,
		autoTimeout: 7000,
		autoPauseOnHover: true,
		showNavigation: true,
		showPagination: true,
		rewindNav: true,
		scrollByPage: true,
		gridAdjustment: 'responsive',
		mediaQueries: [{
			width: 1,
			cols: 1
		}],
		gapHorizontal: 0,
		gapVertical: 0,
		caption: '',
		displayType: 'default'
	};
	$('#cbp-testimonials').cubeportfolio(TestimonialsOptions);



	var ObjectsOptions = {
		layoutMode: 'slider',
		drag: true,
		auto: true,
		autoTimeout: 7000,
		autoPauseOnHover: true,
		showNavigation: true,
		showPagination: false,
		rewindNav: false,
		scrollByPage: true,
		gridAdjustment: 'responsive',
		mediaQueries: [{
			width: 1500,
			cols: 5
		}, {
			width: 1100,
			cols: 4
		}, {
			width: 800,
			cols: 3
		}, {
			width: 480,
			cols: 2
		}, {
			width: 320,
			cols: 1
		}],
		gapHorizontal: 0,
		gapVertical: 0,
		caption: 'overlayBottomAlong',
		displayType: 'default',
		displayTypeSpeed: 400,

		// singlePageInline
		singlePageInlineDelegate: '.cbp-singlePageInline',
		singlePageInlinePosition: 'bottom',
		singlePageInlineInFocus: true,
		singlePageInlineCallback: function(url, element) {
			// to update singlePageInline content use the following method: this.updateSinglePageInline(yourContent)
			var t = this;

			$.ajax({
					url: url,
					type: 'GET',
					dataType: 'html',
					timeout: 10000
				})
				.done(function(result) {

					t.updateSinglePageInline(result);

				})
				.fail(function() {
					t.updateSinglePageInline('AJAX Error! Please refresh the page!');
				});
		}
	};
	$('#cbp-objects').cubeportfolio(ObjectsOptions);
});

$('.wp1').waypoint(function() {
	$('.wp1').addClass('animated fadeInDown');
}, {
	offset: '75%'
});

// initialize Main block 
$('.wp2').waypoint(function() {
	$('.wp2').addClass('animated fadeInDown');
}, {
	offset: '75%'
});
$('.wp2a').waypoint(function() {
	$('.wp2a').addClass('animated fadeInLeft');
}, {
	offset: '75%'
});
$('.wp2b').waypoint(function() {
	$('.wp2b').addClass('animated fadeInRight');
}, {
	offset: '75%'
});

$('.wp3a').waypoint(function() {
	$('.wp3a').addClass('animated fadeInLeft');
}, {
	offset: '75%'
});
$('.wp3b').waypoint(function() {
	$('.wp3b').addClass('animated fadeInRight');
}, {
	offset: '75%'
});


// Функция прикрепляет блок Новостей к низу страницы. Нужно переделать скрипт, чтобы пересчитывал величину отступа при ресайзе. Сейчас ресайз не работает
$(document).ready(function() {

	var $newsbar = $("#news"),
		$window = $(window),
		offset = $newsbar.offset(),
		sbBottom = Math.abs($window.height() - (offset.top + $newsbar.height())),
		prevScrollTop = 0;
	function NewsBlockStick() {
		if (prevScrollTop < $window.scrollTop()) {
			$newsbar.removeClass('fixedTop');
			if ($window.scrollTop() > sbBottom) {
				$newsbar.addClass('fixedBtm');
			} else {
				$newsbar.removeClass('fixedBtm');
			}
		} else {
			$newsbar.removeClass('fixedBtm');
			if ($window.scrollTop() > sbBottom) {
				$newsbar.addClass('fixedTop');
			} else {
				$newsbar.removeClass('fixedTop');
			}
		}
		prevScrollTop = $window.scrollTop();
	}
	$(window).on( 'scroll', NewsBlockStick).trigger('scroll');

});
