$('.wp1').waypoint(function() {
	$('.wp1').addClass('animated fadeInDown');
}, {
	offset: '75%'
});	
$('.wp2').waypoint(function() {
	$('.wp2').addClass('animated fadeInDown');
}, {
	offset: '75%'
});


//вычисляем сумму всех чисел
function summ(e) {
	for(var i=0, sum=0; i<e.length; i++) {
		sum+=parseInt(e[i].value)
	}
	return(sum);
}
//вычисляем процент для конкретного случая a - доля, b - полная сумма, с - уровень точности
function percentage(a,b,c) {
	return Math.round(((100*a)/b) * Math.pow(10, c)) / Math.pow(10, c) ;
}


var dntOptions = {
		segmentShowStroke: true,
		animateRotate: true,
		animateScale: false,
		percentageInnerCutout: 90,
		tooltipFontSize: 10,
		responsive : true,
		showScale: true,
		scaleBeginAtZero: true,
		legendTemplate : "<table class=\"<%=name.toLowerCase()%>Legend\"><% for (var i=0; i<segments.length; i++){%><tr><td><span class=\"legend-pin\" style=\"background-color:<%=segments[i].fillColor%>\"></span></td><td><%if(segments[i].label){%><%=segments[i].label%><%}%>: </td><td style=\"text-align: right;\"><%= numFormat(segments[i].value) %> <i class=\"fa fa-rub\"></i></td></tr><%}%></table>",
		tooltipTemplate: "<%= label %>",
	}
	
	
// --- график 1 --- //

//исходные значения

var Utverjdeno2012 = 792869200;
var Ispolzovano2012 = 171887800;

var dntData2012 = [
		{
			value: Ispolzovano2012,
			color: "#565c6a",
			highlight: "#929daf",
			label: "Использовано"
		},
		{
			value: (Utverjdeno2012 - Ispolzovano2012),
			color: "#F7464A",
			highlight: "#F7464A",
			label: "Недофинансировано"
		},
	];

//запускаем диаграмму
jQuery(document).ready(function($) {
	"use strict";	
	
	//запускаем диаграмму
	var canvas = document.getElementById("chartDiv-2012");
	var ctx = canvas.getContext("2d");
	var myDntChart = new Chart(ctx).Doughnut(dntData2012, dntOptions)
	document.getElementById("legendDiv-2012").innerHTML = myDntChart.generateLegend();

	//вставляем значение процента финансирования (анимируем)
	var percent_number_step = $.animateNumber.numberStepFactories.append('%')
	var decimal_places = 2;
	var decimal_factor = decimal_places === 0 ? 1 : Math.pow(10, decimal_places);
	$('#textDiv-2012 .animNum').animateNumber({ 
		number: percentage(dntData2012[0].value, Utverjdeno2012, decimal_places) * decimal_factor, 
		numberStep: function(now, tween) {
			var floored_number = Math.floor(now) / decimal_factor,
			target = $(tween.elem);
			if (decimal_places > 0) {
				floored_number = floored_number.toFixed(decimal_places);
			}
			target.text(floored_number + '%');
		}
	}, 2000 );
	
	//вставляем значение полной суммы (отформатированное)
	document.getElementById("textDiv-2012").getElementsByClassName("sumNum")[0].innerHTML = numFormat(Utverjdeno2012) + ' <i class="fa fa-rub"></i>';
});

// --- график 2 --- //

//исходные значения
var Utverjdeno2013 = 802869200;
var Ispolzovano2013 = 701887000;

var dntData2013 = [
		{
			value: Ispolzovano2013,
			color: "#565c6a",
			highlight: "#929daf",
			label: "Использовано"
		},
		{
			value: (Utverjdeno2013 - Ispolzovano2013),
			color: "#F7464A",
			highlight: "#F7464A",
			label: "Недофинансировано"
		},
	];

//запускаем диаграмму
jQuery(document).ready(function($) {
	"use strict";	
	
	//запускаем диаграмму
	var canvas = document.getElementById("chartDiv-2013");
	var ctx = canvas.getContext("2d");
	var myDntChart = new Chart(ctx).Doughnut(dntData2013, dntOptions);	
	document.getElementById("legendDiv-2013").innerHTML = myDntChart.generateLegend();
	
	//вставляем значение процента финансирования (анимируем)
	var percent_number_step = $.animateNumber.numberStepFactories.append('%')
	var decimal_places = 2;
	var decimal_factor = decimal_places === 0 ? 1 : Math.pow(10, decimal_places);
	$('#textDiv-2013 .animNum').animateNumber({ 
		number: percentage(dntData2013[0].value, Utverjdeno2013, decimal_places) * decimal_factor, 
		numberStep: function(now, tween) {
			var floored_number = Math.floor(now) / decimal_factor,
			target = $(tween.elem);
			if (decimal_places > 0) {
				floored_number = floored_number.toFixed(decimal_places);
			}
			target.text(floored_number + '%');
		}
	}, 2000 );
	
	//вставляем значение полной суммы (отформатированное)
	document.getElementById("textDiv-2013").getElementsByClassName("sumNum")[0].innerHTML = numFormat(Utverjdeno2013) + ' <i class="fa fa-rub"></i>';
});


// запускаем общий график по годам
var lineData_var1 = {
	labels : ["2012","2013","2014","2015"],
	datasets : [
		{
			label: "Заложено в бюджете",
            pointColor: "rgba(247, 70, 74,1)",
			pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(247, 70, 74,1)",
			fillColor : "rgba(86, 92, 106, 0.1)",
			strokeColor: "rgba(86, 92, 106, 1)",
			highlightStroke: "rgba(86, 92, 106, 1)",
			data : [792869200,802869200]
		}
	]
}			
var lineOptions_var1 = {
		animation : true,
		responsive : true,
		scaleShowLabels : false,
		tooltipTemplate: "<%if (label){%><%=label %> год: <%}%><%= numFormat(value) + ' рублей' %>",
		multiTooltipTemplate: "<%= numFormat(value) + ' рублей' %>"
	}

//запускаем диаграмму
jQuery(document).ready(function($) {
	"use strict";	
	var canvas = document.getElementById("lineDiv-var1");
	var ctx = canvas.getContext("2d");
	var myLineChart = new Chart(ctx).Line(lineData_var1, lineOptions_var1);	
});