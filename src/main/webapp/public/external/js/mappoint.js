
$('.wp1').waypoint(function() {
	$('.wp1').addClass('animated fadeInDown');
}, {
	offset: '75%'
});	
$('.wp2').waypoint(function() {
	$('.wp2').addClass('animated fadeInDown');
}, {
	offset: '75%'
});


// графики
var barData1 = {
	labels : ["2012","2013","2014","2015"],
	datasets : [
		{
			label: "Заложено в бюджете",
			fillColor : "rgba(251, 0, 0,0.5)",
			strokeColor : "rgba(251, 0, 0,0.8)",
			highlightFill: "rgba(251, 0, 0,0.75)",
			highlightStroke: "rgba(251, 0, 0,1)",
			data : [777,888,666,222]
		},
		{
			label: "Реально освоено средств",
			fillColor : "rgba(166, 226, 46,0.5)",
			strokeColor : "rgba(166, 226, 46,0.8)",
			highlightFill : "rgba(166, 226, 46,0.75)",
			highlightStroke : "rgba(166, 226, 46,1)",
			data : [333,444,555,111]
		}
	]
}			
var barOptions1 = {
		animation : true,
		responsive : true,
		barShowStroke : false,
		scaleShowLabels : false,
		legendTemplate : "<table class=\"yandexMap-infoblock-<%=name.toLowerCase()%>Legend\"><% for (var i=0; i<datasets.length; i++){%><tr><td><span class=\"legend-pin\" style=\"background-color:<%=datasets[i].fillColor%>\"></span></td><td><%if(datasets[i].label){%><%=datasets[i].label%><%}%></td></tr><%}%></table>",
		tooltipTemplate: "<%if (label){%><%=label %>: <%}%><%= value + ' рублей' %>",
		multiTooltipTemplate: "<%= value + ' рублей' %>"
	}

//запускаем диаграмму
jQuery(document).ready(function($) {
	"use strict";	
	/*var canvas = document.getElementById("barDiv-var1");
	var ctx = canvas.getContext("2d");
	var myBarChart = new Chart(ctx).Bar(barData1, barOptions1);	
	document.getElementById("legendDiv-var1").innerHTML = myBarChart.generateLegend();*/
});


var barData2 = {
	labels : ["2012","2013","2014","2015"],
	datasets : [
		{
			label: "Заложено в бюджете",
			fillColor : "rgba(251, 0, 0,0.5)",
			strokeColor : "rgba(251, 0, 0,0.8)",
			highlightFill: "rgba(251, 0, 0,0.75)",
			highlightStroke: "rgba(251, 0, 0,1)",
			data : [333,888,666,222]
		},
		{
			label: "Реально освоено средств",
			fillColor : "rgba(166, 226, 46,0.5)",
			strokeColor : "rgba(166, 226, 46,0.8)",
			highlightFill : "rgba(166, 226, 46,0.75)",
			highlightStroke : "rgba(166, 226, 46,1)",
			data : [333,777,555,222]
		}
	]

}			
var barOptions2 = {
		animation : true,
		responsive : true,
		barShowStroke : false,
		scaleShowLabels : false,
		legendTemplate : "<table class=\"yandexMap-infoblock-<%=name.toLowerCase()%>Legend\"><% for (var i=0; i<datasets.length; i++){%><tr><td><span class=\"legend-pin\" style=\"background-color:<%=datasets[i].fillColor%>\"></span></td><td><%if(datasets[i].label){%><%=datasets[i].label%><%}%></td></tr><%}%></table>",
		tooltipTemplate: "<%if (label){%><%=label %>: <%}%><%= value + ' рублей' %>",
		multiTooltipTemplate: "<%= value + ' рублей' %>"

	}

//запускаем диаграмму
jQuery(document).ready(function($) {
	"use strict";	
	/*var canvas = document.getElementById("barDiv-var2");
	var ctx = canvas.getContext("2d");
	var myBarChart = new Chart(ctx).Bar(barData2, barOptions2);	
	document.getElementById("legendDiv-var2").innerHTML = myBarChart.generateLegend();*/
});


				