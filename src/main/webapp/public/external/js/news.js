// initialize Main block 

$('.wp1').waypoint(function() {
	$('.wp1').addClass('animated fadeInDown');
}, {
	offset: '75%'
});	

jQuery(document).ready(function($) {
	"use strict";
	var MasonryOptions = {
        loadMore: '#js-loadMore-list-news',
        loadMoreAction: 'click',
        layoutMode: 'grid',
        gapHorizontal: 0,
        gapVertical: 0,
        gridAdjustment: 'responsive',
		mediaQueries: [{
			width: 1,
			cols: 1
		}],
        displayType: 'default',
        displayTypeSpeed: 100,

        // singlePage popup
        singlePageDelegate: '.cbp-singlePage',
        singlePageDeeplinking: true,
        singlePageStickyNavigation: true,
        singlePageCounter: '<div class="cbp-popup-singlePage-counter">{{current}} of {{total}}</div>',
        singlePageCallback: function(url, element) {
            // to update singlePage content use the following method: this.updateSinglePage(yourContent)
            var t = this;

            $.ajax({
                    url: url,
                    type: 'GET',
                    dataType: 'html',
                    timeout: 10000
                })
                .done(function(result) {
                    t.updateSinglePage(result);
                })
                .fail(function() {
                    t.updateSinglePage('AJAX Error! Please refresh the page!');
                });
        }
	};	
	$('#js-list-news').cubeportfolio(MasonryOptions);
	
});	


$('.wp7').waypoint(function() {
	$('.wp7').addClass('animated fadeInDown');
}, {
	offset: '75%'
});
