var workTablePageState = {
    pageNumber: 1,
    pageSize: 10,
    totalRows: 2,
    sortName: undefined,
    sortOrder: undefined
};

jQuery(document).ready(function ($) {
    correctView();
    renderWorkTable();
});


function correctView() {
    $('.wp1').waypoint(function () {
        $('.wp1').addClass('animated fadeInDown');
    }, {
        offset: '75%'
    });
    $('.wp2').waypoint(function () {
        $('.wp2').addClass('animated fadeInDown');
    }, {
        offset: '75%'
    });
}


function renderWorkTable(data) {
    //Формируем колонки таблицы
    var columns = [];

    columns.push({
        field: 'objectId',
        visible: false
    });
    columns.push({
        field: 'year',
        title: 'Год',
        align: 'left',
        valign: 'top',
        sortable: true,
        formatter: undefined
    });
    columns.push({
        field: 'description',
        title: 'Описание',
        align: 'left',
        valign: 'top',
        sortable: true,
        formatter: undefined
    });
    columns.push({
        field: 'description',
        title: 'Отрасль',
        align: 'left',
        valign: 'top',
        sortable: true,
        formatter: undefined
    });

    columns.push({
        field: 'description',
        title: 'Заказчик',
        align: 'left',
        valign: 'top',
        sortable: true,
        formatter: undefined
    });

    columns.push({
        field: 'description',
        title: 'Запланировно',
        align: 'left',
        valign: 'top',
        sortable: true,
        formatter: undefined
    });

    columns.push({
        field: 'description',
        title: 'Выделено',
        align: 'left',
        valign: 'top',
        sortable: true,
        formatter: undefined
    });

    columns.push({
        field: 'description',
        title: 'Освоено',
        align: 'left',
        valign: 'top',
        sortable: true,
        formatter: undefined
    });

    var pageNumber = 1;
    var pageSize = 10;
    var totalRows = 2;
    var sortName = undefined;
    var sortOrder = undefined;

    var tableData = [];
    tableData.push({objectId: "1", description: "test"});

    $('#data-table').bootstrapTable({
        //toolbar: _this.$toolbar,
        idField: 'objectId',
        data: tableData,
        cache: false,
        striped: true,
        pagination: true,
        pageNumber: workTablePageState.pageNumber,
        pageSize: workTablePageState.pageSize,
        pageList: [5, 10, 20, 50],
        totalRows: totalRows,
        sortName: workTablePageState.sortName,
        sortOrder: workTablePageState.sortOrder,
        showColumns: false,
        /*                    minimumCountColumns: 1,*/ //True to show the columns drop down list.
        search: false,
        clickToSelect: false,
        sidePagination: "server",
        columns: columns
    }).on('page-change.bs.table', function (e, number, size) {
        console.log('Event: page-change.bs.table, data: ' + number + ', ' + size);
        _this.model.changePage(number, size);
    }).on('sort.bs.table', function (e, name, order) {
        console.log('Event: sort.bs.table, data: ' + name + ', ' + order);
        _this.model.changeSort(name, order);
    });
}

function callServerWorkTableData() {
    var _this = this;
    var requestData = {
        action:"getPoints"
    };
    if(year){
        requestData.year = year
    }
    $.ajax(_this.get("context_path")+"/points",{
        type: 'POST',
        data: requestData,
        success: function (responseData) {
            _this.set("coords",responseData.points);
            _this.renderCoords();
        }
    });
}

