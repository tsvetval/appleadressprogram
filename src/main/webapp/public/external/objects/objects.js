
// скрипит вывода сумм по формату 000 000 000
Number.prototype.formatMoney = function(n, x, s, c) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
        num = this.toFixed(Math.max(0, ~~n));
    
    return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
};

//задание переменных
var vsego = 100,										//получаем значение сколько выделено бюджета
var potracheno = 40,									//получаем значение сколько израсходовано
var ostalos = vsego - potracheno, 						//получаем значение сколько денег осталось
var persent = Math.round(vsego%potracheno*100), 		//получение процента эффективности
var values = [
	{
		value: potracheno,
		color:"#F7464A",
		highlight: "#FB0000",
		label: "Потрачено"
	},
	{
		value: ostalos,
		color: "#4D5360",
		highlight: "#323438",
		label: "Осталось"
	}
]
var options = {
	animation: true,
	responsive: true
};

window.onload = function(){
	var ctx = document.getElementById("chart-area").getContext("2d");					//запуск графика
	window.myDoughnut = new Chart(ctx).Doughnut(values, options); 						//запуск графика
	
	........persent + '%';																//прописываем процент в html #persent
	.......(vsego.formatMoney(0, 3, ' ', ',') + '<i class="fa fa-rub"></i>'); 			//прописываем значение бюджета в html #income
	.......(potracheno.formatMoney(0, 3, ' ', ',') + '<i class="fa fa-rub"></i>'); 		//прописываем значение освоенной части бюджета в html #cost
};
//Короче, тут выше нужно написать функцию, чтобы после подгрузки этой байды по ajax на главную срабатывали скрипты и прописывались значения в отведенные поля. В общем, я хз как вы будете делать, заморачиваться не стал