/*
* Создает шаблон лоя pageBlock(JS для Backbone и Java контроллер)
* Контроллер: ru.peak.ml.web.block.controller.impl.PageWizardBlockController
* */


define(
    ['log', 'misc', 'backbone',
        'apple/blocks/import_invest_block/model/ImportBlockModel',
        'apple/blocks/import_invest_block/view/ImportBlockView'],
    function (log, misc, backbone, model, view) {
        return {
            model: model,
            view: view
        };
    });
