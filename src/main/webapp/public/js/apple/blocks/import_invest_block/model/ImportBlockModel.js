define(
    ['log', 'misc', 'backbone', 'cms/events/events',
        'cms/model/PageBlockModel', 'cms/page_blocks/DialogPageBlock'
    ],
    function (log, misc, backbone, cmsEvents, PageBlockModel, Message) {
        var model = PageBlockModel.extend({

            defaults: {
                pageTitle: 'Импорт объектов'
            },

            initialize: function () {
                /* PageBlockModel.initialize() для установки событий */
                model.__super__.initialize.call(this);

                console.log("initialize BlockModel");
            },

            /**
             * Добавление событий блока
             * @Override PageBlockModel.bindEvents
             */
            bindEvents: function () {
                /* PageBlockModel.bindEvents() добавляет основные события блока */
                model.__super__.bindEvents.call(this);

                var _this = this;
                this.listenTo(this, cmsEvents.RESTORE_PAGE, function (params) {
                    _this.trigger(cmsEvents.RENDER_VIEW);
                });
            },

            /**
             * Остановка обработчиков событий блока
             * @Override PageBlockModel.unbindEvents
             */
            unbindEvents: function () {
                /*  События блока */
                this.stopListening(this, cmsEvents.RESTORE_PAGE);

                /* PageBlockModel.unbindEvents() удаляет все основные события блока */
                model.__super__.unbindEvents.call(this)
            },


            importInvestData: function (file) {
                var _this = this;
                this.uploadFile(file, function (filename) {
                    _this.import(filename);
                    //_this.updateStatus();
                });
            },

            uploadFile: function (file, callback) {
                var _this = this;
                console.log('Start uploading file: ' + file);
                var xhr = new XMLHttpRequest();
                var url = misc.getContextPath() + "/upload";
                xhr.onreadystatechange = function (e) {
                    if (4 == this.readyState) {
                        console.log('File uploaded: ' + file.name);
                        callback.apply(_this, [file.name]);
                    }
                };
                xhr.open('post', url, true);
                var fd = new FormData;
                fd.append(file.name, file);
                xhr.send(fd);
            },

            import: function (filename) {
                var _this = this;
                var options = {
                    url: misc.getContextPath() + "/import/investitems/doimport",
                    action: 'doImport',
                    ml_request: true,
                    data: {
                        filename: filename
                    }
                };
                _this.callServerAction(options).then(function (result) {

                    debugger;
                    _this.set("importResult", JSON.parse(result.result));
                    _this.trigger("renderDone");
                    if (result.status == "OK") {
                        alert('Ok result');
                    }
                })
            }

        });
        return model;
    });
