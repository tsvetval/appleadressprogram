<h2>
    Импорт Объектов Инвестиционной программы
</h2>

<h4>Требования к входному файлу:</h4>
<div id="import-info">
    <ul>
        <li>Название файла для импорта должно состоять из латинских букв и цифр и не содержать пробелов и спецсимволов</li>
        <li>Первое поле файла: год</li>
        <li>Лист должен содержать информацию в следующих столбцах:
            <ol>
                <li>Наименование показателя</li>
                <li>Утверждено на год</li>
                <li>Профинансировано за период</li>
                <li>% исполнения</li>
            </ol>
        </li>
    </ul>
</div>
<div id="import-div">
    <input id="import-file" name="import-file" type="file"/>
    <label id="import-file-label" for="import-file">Нажмите, чтобы выбрать файл для импорта</label>
</div>

<div id="import-buttons">
    <div class="btn btn-primary" id="doImport">Импорт</div>
    <div class="btn btn-default" id="cancelImport">Отмена</div>
</div>

<div id="import-process" class="import-process" style="display: none;">
    Подождите! Идёт импорт данных...
</div>
<div id="import-status" class="import-status"></div>
