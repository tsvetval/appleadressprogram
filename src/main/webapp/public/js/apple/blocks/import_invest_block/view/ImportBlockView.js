define(
    ['log', 'misc', 'backbone', 'cms/view/PageBlockView', 'markup', 'cms/page_blocks/DialogPageBlock', 'moment',
        'text!apple/blocks/import_invest_block/template/init.tpl',
        'text!apple/blocks/import_invest_block/template/done.tpl'],
    function (log, misc, backbone, PageBlockView, markup, Message, moment,
              initTemplate, doneTemplate) {

        var view = PageBlockView.extend({

            events: {
                "click #doImport": "importClick"
            },

            initialize: function () {
                this.listenTo(this.model, 'render', this.render);
                this.listenTo(this.model, 'renderDone', this.renderDone);
            },

            render: function () {
                var _this = this;
                this.$el.html(_.template(initTemplate, {model: this.model}));
            },

            renderDone: function () {
                var _this = this;
                this.$el.html(_.template(doneTemplate, {importedCount: this.model.get('importResult').importedCount}));
            },

            importClick: function () {
                var _this = this;
                var file = _this.$el.find('#import-file')[0].files[0];
                if (file) {
                    _this.model.importInvestData(file);
                    _this.$el.find('#import-div').hide();
                    _this.$el.find('#import-buttons').hide();
                    _this.$el.find('#delete-buttons').hide();
                    _this.$el.find('#import-process').show();
                } else {
                    // TODO _this.showEshowError("Не задан файл импорта!");
                }
            }


        });
        return view;
    });
