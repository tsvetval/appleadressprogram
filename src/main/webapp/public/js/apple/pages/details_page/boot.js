jQuery(document).ready(function($){
            correctView();
            var siteModel = new model();
            var siteView = new view({model: siteModel, el: $("#intro")});
            siteModel.trigger('restorePage');
});

function correctView() {
    $('.wp1').waypoint(function () {
        $('.wp1').addClass('animated fadeInDown');
    }, {
        offset: '75%'
    });
    $('.wp2').waypoint(function () {
        $('.wp2').addClass('animated fadeInDown');
    }, {
        offset: '75%'
    });
}