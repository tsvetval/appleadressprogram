
        var model = Backbone.Model.extend({
            defaults: {
                coodrs: undefined,
                context_path:undefined,
                map:undefined
            },

            initialize: function (param) {
                var _this = this;
                this.set("context_path",$('body').data("context"));
                this.listenTo(this, 'restorePage', function () {
                    _this.getAllData();
                });

            },
            getAllData: function(){
                var _this = this;
                var requestData = {
                    action:"getWorkInfo",
                    workId:$('body').data("id")
                }

                $.ajax(_this.get("context_path")+"/object/details",{
                    type: 'POST',
                    data: requestData,
                    success: function (responseData) {
                        _this.set("work",responseData.work);
                        if(responseData.work.points.length >0) {
                            var center = responseData.work.points.length > 0 ? eval(responseData.work.points[0].coord) : [59.9190, 30.3529];
                            ymaps.ready(function () {
                                var myMap = new ymaps.Map("map", {
                                    center: center,
                                    zoom: 15,
                                    controls: ["zoomControl"]
                                });
                                _this.set("map", myMap);
                                var myClusterer = new ymaps.Clusterer();
                                var myGeoObjects = [];
                                responseData.work.points.forEach(function (coord) {
                                    var myPlacemark1 = new ymaps.Placemark(JSON.parse(coord.coord), {hintContent: coord.title});
                                    myPlacemark1.events.add("click", function (e) {
                                        _this.getPointInfo(this.id);
                                    }, coord)
                                    myGeoObjects[myGeoObjects.length] = myPlacemark1;
                                });
                                myClusterer.add(myGeoObjects);
                                myMap.geoObjects.removeAll();
                                myMap.geoObjects.add(myClusterer);
                            });
                        }
                        _this.trigger("render");
                    }
                });
            }
        });
