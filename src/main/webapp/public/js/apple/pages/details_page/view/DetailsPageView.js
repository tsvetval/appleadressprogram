var view = Backbone.View.extend({
    initialize: function () {
        var _this = this;

        this.listenTo(this.model, 'render', this.render);
        this.listenTo(this.model, 'renderPointInfo', this.renderPointInfo);
        this.$el = $('#content');

    },
    percentage: function(a,b,c) {
        if(b == 0){
            return 0;
        }else {
            return Math.round(((100 * a) / b) * Math.pow(10, c)) / Math.pow(10, c);
        }
    },
    render: function () {
        var _this = this;
        var work = this.model.get("work");
        $('#workHeader').html(work.name);
        var template = $("#customerDiagram").html();
        $('#point-graphics').html(_.template(template, {customers: work.custs}));
        work.custs.forEach(function (customer) {
            var dntOptions = {
                segmentShowStroke: true,
                animateRotate: true,
                animateScale: false,
                percentageInnerCutout: 90,
                tooltipFontSize: 10,
                responsive: true,
                showScale: true,
                scaleBeginAtZero: true,
                legendTemplate: "<table class=\"<%=name.toLowerCase()%>Legend\"><% for (var i=0; i<segments.length; i++){%><tr><td><span class=\"legend-pin\" style=\"background-color:<%=segments[i].fillColor%>\"></span></td><td><%if(segments[i].label){%><%=segments[i].label%><%}%>: </td><td style=\"text-align: right;\"><%= numFormat(segments[i].value) %> <i class=\"fa fa-rub\"></i></td></tr><%}%></table>",
                tooltipTemplate: "<%= label %>",
            };

            customer.years.forEach(function (year) {
                year.moneyUsed = year.moneyUsed * 1000;
                year.moneyAllocated = year.moneyAllocated * 1000;
                var dntData = [
                    {
                        value: year.moneyUsed,
                        color: "#565c6a",
                        highlight: "#929daf",
                        label: "Использовано"
                    },
                    {
                        value: Math.round(year.moneyAllocated - year.moneyUsed),
                        color: "#F7464A",
                        highlight: "#F7464A",
                        label: "Недофинансировано"
                    }
                ];
                var canvas = document.getElementById("chartDiv-"+customer.id+"-"+year.year);
                var ctx = canvas.getContext("2d");
                var myDntChart = new Chart(ctx).Doughnut(dntData, dntOptions)
                document.getElementById("legendDiv-"+customer.id+"-"+year.year).innerHTML = myDntChart.generateLegend();

                //вставляем значение процента финансирования (анимируем)
                var percent_number_step = $.animateNumber.numberStepFactories.append('%');
                var decimal_places = 2;
                var decimal_factor = decimal_places === 0 ? 1 : Math.pow(10, decimal_places);
                $('#textDiv-'+customer.id+"-"+year.year+' .animNum').animateNumber({
                    number: _this.percentage(dntData[0].value, year.moneyAllocated, decimal_places) * decimal_factor,
                    numberStep: function (now, tween) {
                        var floored_number = Math.floor(now) / decimal_factor,
                            target = $(tween.elem);
                        if (decimal_places > 0) {
                            floored_number = floored_number.toFixed(decimal_places);
                        }
                        target.text(floored_number + '%');
                    }
                }, 2000);

                //вставляем значение полной суммы (отформатированное)
                document.getElementById("textDiv-"+customer.id+"-"+year.year).getElementsByClassName("sumNum")[0].innerHTML = numFormat(year.moneyAllocated) + ' <i class="fa fa-rub"></i>';
            });

            customer.allAmount = _.map(customer.allAmount, function (item) { return item * 1000 });

            var lineData_var = {
                labels : customer.allYears,
                datasets : [
                    {
                        label: "Заложено в бюджете",
                        pointColor: "rgba(247, 70, 74,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(247, 70, 74,1)",
                        fillColor : "rgba(86, 92, 106, 0.1)",
                        strokeColor: "rgba(86, 92, 106, 1)",
                        highlightStroke: "rgba(86, 92, 106, 1)",
                        data : customer.allAmount
                    }
                ]
            };
            var lineOptions_var = {
                animation : true,
                responsive : true,
                scaleShowLabels : false,
                tooltipTemplate: "<%if (label){%><%=label %> год: <%}%><%= numFormat(value) + ' рублей' %>",
                multiTooltipTemplate: "<%= numFormat(value) + ' рублей' %>"
            };
            var canvas = document.getElementById("lineDiv-var"+customer.id);
            var ctx = canvas.getContext("2d");
            var myLineChart = new Chart(ctx).Line(lineData_var, lineOptions_var);
        });
    }
});
