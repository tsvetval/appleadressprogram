var model = Backbone.Model.extend({
    defaults: {
        objectDataList: undefined,
        // номер текущей страницы списка для отображения
        currentPage: undefined,
        // число объектов для отображения на странице
        objectsPerPage: undefined,
        // общее число объектов в списке
        recordsCount: undefined,
        // атрибут для сортировки
        orderAttr: undefined,
        // тип сортировки
        orderType: undefined,

        filterDefaults: {},
        filterValues: {}
    },

    initialize: function (param) {
        var _this = this;
        this.set("context_path", $('body').data("context"));
        this.listenTo(this, 'restorePage', function () {

            $.when(this.getFilterParemeters(), this.getObjectListData()).then(function (filterDataResp, tableDataResp) {
                    var tableData = tableDataResp[0];
                    var filterData = filterDataResp[0];
                    _this.set("objectDataList", tableData.objectDataList);
                    _this.set("recordsCount", tableData.RecordsCount);

                    _this.get('filterDefaults').years = filterData.years;
                    _this.get('filterDefaults').customers = filterData.customers;
                    _this.get('filterDefaults').branches = filterData.branches;
                    _this.get('filterDefaults').districts = filterData.districts;
                    _this.trigger("render");
                    //_this.trigger("updateTableData");
                }
            );
            /*
             _this.getFilterParemeters();
             _this.getObjectListData();
             */
        });

    },

    getFilterParemeters: function () {
        var _this = this;
        var requestData = {
            action: "getFilterParameters",
        };
        return $.ajax(_this.get("context_path") + "/object/list/works", {
            type: 'POST',
            data: requestData
            /*
             success: function (responseData) {
             _this.set("objectDataList", responseData.objectDataList);
             _this.set("recordsCount", responseData.RecordsCount);
             _this.trigger("renderFilterParameters");
             }
             */
        });
    },


    getObjectListData: function () {
        var _this = this;

        var requestData = {
            action: "getWorkList",
            currentPage: this.get("currentPage"),
            objectsPerPage: this.get("objectsPerPage"),
            orderAttr: this.get("orderAttr"),
            orderType: this.get("orderType"),
            filterValues : _this._getFilterParams()
        };
        return $.ajax(_this.get("context_path") + "/object/list/works", {
            type: 'POST',
            data: requestData
            /*
             success: function (responseData) {
             _this.set("objectDataList", responseData.objectDataList);
             _this.set("recordsCount", responseData.RecordsCount);
             _this.trigger("updateTableData");
             }
             */
        });
    },

    _getFilterParams: function () {
        var filterArray = [];
        if (this.get("filterValues").year) {
            filterArray.push({
                "entityName": "year", "op": "equals", "value": this.get("filterValues").year})
        }
        if (this.get("filterValues").customer){
            filterArray.push(
                {"entityName":"customer","children":[{"entityName":"id","op":"equals","value": this.get("filterValues").customer}]})
        }
        if (this.get("filterValues").branch){
            filterArray.push(
             {"entityName":"branch","children":[{"entityName":"id","op":"equals","value":this.get("filterValues").branch}]})
        }
        if (this.get("filterValues").district){
            filterArray.push(
                {"entityName":"district","children":[{"entityName":"id","op":"equals","value":this.get("filterValues").district}]})
        }


        if (filterArray.length > 0) {
            return JSON.stringify(filterArray);
        } else {
            return undefined;
        }
    },

    /**
     * Смена текущей страницы/количества объектов на странице
     *
     * @param page              -   номер текущеей страницы
     * @param objectsPerPage    -   число объектов для отображения на странице
     */
    changePage: function (page, objectsPerPage) {
        var _this = this;
        this.set("currentPage", page);
        this.set("objectsPerPage", objectsPerPage);
        $.when(this.getObjectListData()).then(function (responseData) {
            _this.set("objectDataList", responseData.objectDataList);
            _this.set("recordsCount", responseData.RecordsCount);
            _this.trigger("updateTableData");
        });
    },

    /**
     * Смена сортировки списка объектов
     *
     * @param orderAttr     -   атрибут для сортировки
     * @param orderType     -   тип сортировки (asc/desc)
     */
    changeSort: function (orderAttr, orderType) {
        var _this = this;
        this.set("orderAttr", orderAttr);
        this.set("orderType", orderType);
        $.when(this.getObjectListData()).then(function (responseDataResp) {
            _this.set("objectDataList", responseDataResp.objectDataList);
            _this.set("recordsCount", responseDataResp.RecordsCount);
            _this.trigger("updateTableData");
        });
    },

    changeFilter: function () {
        var _this = this;
        $.when(this.getObjectListData()).then(function (responseData) {
            _this.set("objectDataList", responseData.objectDataList);
            _this.set("recordsCount", responseData.RecordsCount);
            _this.trigger("updateTableData");
        });
    }

});
