var view = Backbone.View.extend({
    events: {
        "click #changeRider": "showReaderListForChoice",
        "change #year": "yearChange",
        "click #getCPLC": "getCPLC"
    },

    initialize: function () {
        var _this = this;
        this.listenTo(this.model, 'render', this.render);
        this.listenTo(this.model, 'updateTableData', this.updateTableData);
        this.listenTo(this.model, 'renderFilterParameters', this.renderFilterParameters);
        /*this.$el = $('#content')*/
        this.$table = $('#data-table');
    },



    updateTableData: function () {
        this.$table.bootstrapTable('load', {
            total : this.model.get('recordsCount'),
            rows  : this.model.get('objectDataList')
        })
    },

    render: function () {
        var _this = this;
        _this.$el.find("#point-intro").show();

        // init year
        this.$yearField = this.$el.find('#yearFilter');
        this.$customerField = this.$el.find('#customerFilter');
        this.$branchField = this.$el.find('#branchFilter');
        this.$districtField = this.$el.find('#districtFilter');

        var select2Data = [];
        this.model.get('filterDefaults').years.forEach(function (year) {
            select2Data.push({id: year, text: year});
        });
        this.$yearField.select2({
            width: '100%',
            data: select2Data,
            allowClear: true,
            placeholder: "Выберите значение"
        });
        this.$yearField.val(null).trigger("change");
        this.$yearField.on("change", function () {
            _this.model.get("filterValues").year = _this.$yearField.val();
            _this.model.changeFilter();

        });

        _this.$customerField.select2({
            width: '100%',
            data: this.model.get('filterDefaults').customers,
            allowClear: true,
            placeholder: "Выберите значение"
        });
        this.$customerField.val(null).trigger("change");
        this.$customerField.on("change", function () {
            _this.model.get("filterValues").customer = _this.$customerField.val();
            _this.model.changeFilter();

        });

        this.$branchField.select2({
            width: '100%',
            data: this.model.get('filterDefaults').branches,
            allowClear: true,
            placeholder: "Выберите значение"
        });
        this.$branchField.val(null).trigger("change");
        this.$branchField.on("change", function () {
            _this.model.get("filterValues").branch = _this.$branchField.val();
            _this.model.changeFilter();

        });

        this.$districtField.select2({
            width: '100%',
            data: this.model.get('filterDefaults').districts,
            allowClear: true,
            placeholder: "Выберите значение"
        });
        this.$districtField.val(null).trigger("change");
        this.$districtField.on("change", function () {
            _this.model.get("filterValues").district = _this.$districtField.val();
            _this.model.changeFilter();

        });



        //Формируем колонки таблицы
        var columns = [];

        columns.push({
            field: 'id',
            visible: false
        });
        columns.push({
            field: 'year',
            title: 'Год',
            align: 'left',
            valign: 'top',
            sortable: true,
            formatter: undefined
        });
        columns.push({
            field: 'litleName',
            title: 'Описание',
            align: 'left',
            valign: 'top',
            sortable: false,
            formatter: _this.openWorkFormatter
        });
        /*        columns.push({
         field: 'description',
         title: 'Отрасль',
         align: 'left',
         valign: 'top',
         sortable: true,
         formatter: undefined
         });

         columns.push({
         field: 'description',
         title: 'Заказчик',
         align: 'left',
         valign: 'top',
         sortable: true,
         formatter: undefined
         });*/

        columns.push({
            field: 'plane_amount',
            title: 'Запланировно(млн)',
            align: 'left',
            valign: 'top',
            sortable: true,
            formatter: undefined
        });

        columns.push({
            field: 'allocated_amount',
            title: 'Выделено(млн)',
            align: 'left',
            valign: 'top',
            sortable: true,
            formatter: undefined
        });

        columns.push({
            field: 'percent',
            title: 'Освоено %',
            align: 'left',
            valign: 'top',
            sortable: true,
            formatter: undefined
        });

        this.$table.bootstrapTable({
            //toolbar: _this.$toolbar,
            idField: 'id',
            data: this.model.get('objectDataList'),
            cache: false,
            striped: true,
            pagination: true,
            pageNumber: this.model.get('currentPage'),
            pageSize: this.model.get('objectsPerPage'),
            pageList: [5, 10, 20, 50],
            totalRows: this.model.get('recordsCount'),
            sortName: this.model.get('orderAttr'),
            sortOrder: this.model.get('orderType'),
            showColumns: false,
            /*                    minimumCountColumns: 1,*/ //True to show the columns drop down list.
            search: false,
            clickToSelect: false,
            sidePagination: "server",
            columns: columns
        }).on('page-change.bs.table', function (e, number, size) {
            console.log('Event: page-change.bs.table, data: ' + number + ', ' + size);
            _this.model.changePage(number, size);
        }).on('sort.bs.table', function (e, name, order) {
            console.log('Event: sort.bs.table, data: ' + name + ', ' + order);
            _this.model.changeSort(name, order);
        });

        _this.$el.find("#point-intro").show();
    },

    openWorkFormatter : function(value, row, index){
       // var context = $("body").attr('data-context');
        var result =
            '<a style="color: blue" href="../points/details?id='+ row.id +'" target="_blank">' +  value +
            '</a>';
        return result;
    }

});
