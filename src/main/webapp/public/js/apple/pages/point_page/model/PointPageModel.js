var model = Backbone.Model.extend({
    defaults: {
        coodrs: undefined,
        context_path: undefined,
        map: undefined,
        filterDefaults: {},
        filterValues: {}
    },

    initialize: function (param) {
        var _this = this;
        this.set("context_path", $('body').data("context"));
        this.listenTo(this, 'restorePage', function () {
            _this.initMap();
            _this.loadTable();
            _this.trigger("render");
        });

    },
    initMap: function () {
        var _this = this;
        ymaps.ready(function () {
            var myMap = new ymaps.Map("map", {
                center: [59.9190, 30.3529],
                zoom: 10,
                controls: ["zoomControl"]
            });
            _this.set("map", myMap);
            //_this.getCoords();
        });

    },
    /*getCoords: function(year){
     var _this = this;
     var requestData = {
     action:"getPoints"
     };
     if(year){
     requestData.year = year
     }
     $.ajax(_this.get("context_path")+"/points",{
     type: 'POST',
     data: requestData,
     success: function (responseData) {
     _this.set("coords",responseData.points);
     _this.renderCoords();
     }
     });
     },*/
    renderCoords: function () {
        var _this = this;
        ymaps.ready(function () {
            var myClusterer = new ymaps.Clusterer();
            var myGeoObjects = [];
            _this.get("coords").forEach(function (coord) {
                var myPlacemark1 = new ymaps.Placemark(JSON.parse(coord.coord), {hintContent: coord.title});
                myPlacemark1.events.add("click", function (e) {
                    _this.getPointInfo(this.id);
                }, coord)
                myGeoObjects[myGeoObjects.length] = myPlacemark1;
            });
            myClusterer.add(myGeoObjects);
            _this.get("map").geoObjects.removeAll();
            _this.get("map").geoObjects.add(myClusterer);
        });
    },
    getPointInfo: function (id) {
        var _this = this;
        var requestData = {
            action: "getPointInfo",
            pointId: id
        };
        $.ajax(_this.get("context_path") + "/points", {
            type: 'POST',
            data: requestData,
            success: function (responseData) {
                _this.set("pointInfo", responseData.pointInfo);
                _this.trigger("renderPointInfo");
            }
        });
    },
    _getFilterParams: function () {
        var filterArray = [];
        if (this.get("filterValues").year) {
            filterArray.push({
                "entityName": "details.year", "op": "equals", "value": this.get("filterValues").year
            })
        }
        if (this.get("filterValues").district) {
            filterArray.push({
                "entityName":"details.district","children":[{"entityName":"id","op":"equals","value":this.get("filterValues").district}]})
        }
        if (this.get("filterValues").pointType) {
            filterArray.push({
                "entityName": "pointType", "children":[{"entityName":"id","op":"equals","value":this.get("filterValues").pointType}]
            })
        }
        if (filterArray.length > 0) {
            return JSON.stringify(filterArray);
        } else {
            return undefined;
        }
    },
    loadTable: function () {
        var _this = this;
        $.when(this.getFilterParemeters(), this.getObjectListData()).then(function (filterDataResp, tableDataResp) {
                var tableData = tableDataResp[0];
                var filterData = filterDataResp[0];
                _this.set("objectDataList", tableData.objectDataList);
                _this.set("recordsCount", tableData.RecordsCount);

                _this.get('filterDefaults').years = filterData.years;
                _this.get('filterDefaults').districts = filterData.districts;
                _this.get('filterDefaults').pointTypes = filterData.pointTypes;
                _this.set("coords", tableData.points);
                _this.renderCoords();
                _this.trigger("renderTable");
                //_this.trigger("updateTableData");
            }
        );
    },
    getFilterParemeters: function () {
        var _this = this;
        var requestData = {
            action: "getFilterParameters",
        };
        return $.ajax(_this.get("context_path") + "/points", {
            type: 'POST',
            data: requestData
            /*
             success: function (responseData) {
             _this.set("objectDataList", responseData.objectDataList);
             _this.set("recordsCount", responseData.RecordsCount);
             _this.trigger("renderFilterParameters");
             }
             */
        });
    },
    getObjectListData: function () {
        var _this = this;

        var requestData = {
            action: "getPointList",
            currentPage: this.get("currentPage"),
            objectsPerPage: this.get("objectsPerPage"),
            orderAttr: this.get("orderAttr"),
            orderType: this.get("orderType"),
            filterValues: _this._getFilterParams()
        };
        return $.ajax(_this.get("context_path") + "/points", {
            type: 'POST',
            data: requestData,
            /*
             success: function (responseData) {
             _this.set("objectDataList", responseData.objectDataList);
             _this.set("recordsCount", responseData.RecordsCount);
             _this.trigger("updateTableData");
             }
             */
        });
    },
    changeFilter: function () {
        var _this = this;
        $.when(this.getObjectListData()).then(function (responseData) {
            _this.set("objectDataList", responseData.objectDataList);
            _this.set("recordsCount", responseData.RecordsCount);
            _this.set("coords", responseData.points);
            _this.renderCoords();
            _this.trigger("updateTableData");
        });
    },

    /**
     * Смена сортировки списка объектов
     *
     * @param orderAttr     -   атрибут для сортировки
     * @param orderType     -   тип сортировки (asc/desc)
     */
    changeSort: function (orderAttr, orderType) {
        var _this = this;
        this.set("orderAttr", orderAttr);
        this.set("orderType", orderType);
        $.when(this.getObjectListData()).then(function (responseDataResp) {
            _this.set("objectDataList", responseDataResp.objectDataList);
            _this.set("recordsCount", responseDataResp.RecordsCount);
            _this.trigger("updateTableData");
        });
    },

    /**
     * Смена текущей страницы/количества объектов на странице
     *
     * @param page              -   номер текущеей страницы
     * @param objectsPerPage    -   число объектов для отображения на странице
     */
    changePage: function (page, objectsPerPage) {
        var _this = this;
        this.set("currentPage", page);
        this.set("objectsPerPage", objectsPerPage);
        $.when(this.getObjectListData()).then(function (responseData) {
            _this.set("objectDataList", responseData.objectDataList);
            _this.set("recordsCount", responseData.RecordsCount);
            _this.trigger("updateTableData");
        });
    }

});
