var view = Backbone.View.extend({
    events: {
        "change #year": "yearChange"

    },

    initialize: function () {
        var _this = this;

        this.listenTo(this.model, 'render', this.render);
        this.listenTo(this.model, 'renderTable', this.renderTable);
        this.listenTo(this.model, 'renderPointInfo', this.renderPointInfo);
        this.listenTo(this.model, 'updateTableData', this.updateTableData);
        this.$el = $('#table');
        this.$table = $('#data-table');

    },
    updateTableData: function () {
        this.$table.bootstrapTable('load', {
            total: this.model.get('recordsCount'),
            rows: this.model.get('objectDataList')
        })
    },
    yearChange: function () {
        this.model.getCoords(this.$el.find("#year").val());
    },
    renderPointInfo: function () {
        var _this = this;

        var pointInfo = this.model.get("pointInfo");
        var template = $("#customers").html();
        $('#pointInfo').html(_.template(template, {pointInfo: pointInfo}));
        $('#pointInfo').parent().css("display", "block");
        pointInfo.customers.forEach(function (customer) {

            customer.allAmount = _.map(customer.allAmount, function (item) { return item * 1000 });
            customer.doneAmount = _.map(customer.doneAmount, function (item) { return item * 1000 });

            var barData1 = {
                labels: _.sortBy(customer.years, function (year) { return year }),
                datasets: [
                    {
                        label: "Заложено в бюджете",
                        fillColor: "rgba(251, 0, 0,0.5)",
                        strokeColor: "rgba(251, 0, 0,0.8)",
                        highlightFill: "rgba(251, 0, 0,0.75)",
                        highlightStroke: "rgba(251, 0, 0,1)",
                        data: customer.allAmount
                    },
                    {
                        label: "Реально освоено средств",
                        fillColor: "rgba(166, 226, 46,0.5)",
                        strokeColor: "rgba(166, 226, 46,0.8)",
                        highlightFill: "rgba(166, 226, 46,0.75)",
                        highlightStroke: "rgba(166, 226, 46,1)",
                        data: customer.doneAmount
                    }
                ]
            }
            var barOptions1 = {
                animation: true,
                responsive: true,
                barShowStroke: false,
                scaleShowLabels: false,
                legendTemplate: "<table class=\"yandexMap-infoblock-<%=name.toLowerCase()%>Legend\"><% for (var i=0; i<datasets.length; i++){%><tr><td><span class=\"legend-pin\" style=\"background-color:<%=datasets[i].fillColor%>\"></span></td><td><%if(datasets[i].label){%><%=datasets[i].label%><%}%></td></tr><%}%></table>",
                tooltipTemplate: "<%if (label){%><%=label %>: <%}%><%= value + ' рублей' %>",
                multiTooltipTemplate: "<%= value + ' рублей' %>"
            }
            var canvas = document.getElementById("barDiv-var-" + customer.id);
            var ctx = canvas.getContext("2d");
            var myBarChart = new Chart(ctx).Bar(barData1, barOptions1);
            $("#legendDiv-var-" + customer.id).html(myBarChart.generateLegend());

        });
        $("#close").on("click", function(){_this.closeWindow();})
    },
    closeWindow: function(){
        $('#pointInfo').parent().css("display", "none");
    },

    renderTable: function () {
        var _this = this;
        _this.$el.find("#point-intro").show();

        // init year
        this.$inputField = this.$el.find('#yearFilter');
        this.$districtField = this.$el.find('#districtFilter');
        this.$pointTypeFilter = this.$el.find('#pointTypeFilter');

        var select2Data = [];
        this.model.get('filterDefaults').years.forEach(function (year) {
            select2Data.push({id: year, text: year});
        });
        _this.$inputField.select2({
            width: '100%',
            data: select2Data,
            allowClear: true,
            placeholder: "Выберите значение"
        });

        _this.$inputField.val(null).trigger("change");

        this.$inputField.on("change", function () {
            _this.model.get("filterValues").year = _this.$inputField.val();
            _this.model.changeFilter();

        });

        this.$districtField.select2({
            width: '100%',
            data: this.model.get('filterDefaults').districts,
            allowClear: true,
            placeholder: "Выберите значение"
        });
        this.$districtField.val(null).trigger("change");
        this.$districtField.on("change", function () {
            _this.model.get("filterValues").district = _this.$districtField.val();
            _this.model.changeFilter();

        });

        this.$pointTypeFilter.select2({
            width: '100%',
            data: this.model.get('filterDefaults').pointTypes,
            allowClear: true,
            placeholder: "Выберите значение"
        });
        this.$pointTypeFilter.val(null).trigger("change");
        this.$pointTypeFilter.on("change", function () {
            _this.model.get("filterValues").pointType = _this.$pointTypeFilter.val();
            _this.model.changeFilter();

        });



        //Формируем колонки таблицы
        var columns = [];

        columns.push({
            field: 'id',
            visible: false
        });
        columns.push({
            field: 'year',
            title: 'Год',
            align: 'left',
            valign: 'top',
            formatter: undefined
        });
        columns.push({
            field: 'name',
            title: 'Описание',
            align: 'left',
            valign: 'top',
            sortable: false,
            formatter: function (value, object) {
                return _.template('<a href="<%= context %>/page/points/works?id=<%= object.id %>" target="_blank"><%= object.name %></a>', {object: object, context: $('body').data('context')})
            }
        });
        /*        columns.push({
         field: 'description',
         title: 'Отрасль',
         align: 'left',
         valign: 'top',
         sortable: true,
         formatter: undefined
         });

         columns.push({
         field: 'description',
         title: 'Заказчик',
         align: 'left',
         valign: 'top',
         sortable: true,
         formatter: undefined
         });*/

        columns.push({
            field: 'plane_amount',
            title: 'Запланировно',
            align: 'left',
            valign: 'top',
            sortable: false,
            formatter: function (value) {
                return value * 1000
            }
        });

        columns.push({
            field: 'allocated_amount',
            title: 'Выделено',
            align: 'left',
            valign: 'top',
            sortable: false,
            formatter: function (value) {
                return value * 1000
            }
        });

        columns.push({
            field: 'percent',
            title: 'Освоено %',
            align: 'left',
            valign: 'top',
            sortable: false,
            formatter: undefined
        });

        this.$table.bootstrapTable({
            //toolbar: _this.$toolbar,
            idField: 'id',
            data: this.model.get('objectDataList'),
            cache: false,
            striped: true,
            pagination: true,
            pageNumber: this.model.get('currentPage'),
            pageSize: this.model.get('objectsPerPage'),
            pageList: [5, 10, 20, 50],
            totalRows: this.model.get('recordsCount'),
            sortName: this.model.get('orderAttr'),
            sortOrder: this.model.get('orderType'),
            showColumns: false,
            /*                    minimumCountColumns: 1,*/ //True to show the columns drop down list.
            search: false,
            clickToSelect: false,
            sidePagination: "server",
            columns: columns
        }).on('page-change.bs.table', function (e, number, size) {
            console.log('Event: page-change.bs.table, data: ' + number + ', ' + size);
            _this.model.changePage(number, size);
        }).on('sort.bs.table', function (e, name, order) {
            console.log('Event: sort.bs.table, data: ' + name + ', ' + order);
            _this.model.changeSort(name, order);
        });

        _this.$el.find("#point-intro").show();
    }

});
