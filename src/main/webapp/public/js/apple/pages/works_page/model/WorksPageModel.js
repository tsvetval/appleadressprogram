
var model = Backbone.Model.extend({
    defaults: {
        coodrs: undefined,
        context_path:undefined,
        map:undefined
    },

    initialize: function (param) {
        var _this = this;
        this.set("context_path",$('body').data("context"));
        this.listenTo(this, 'restorePage', function () {
            _this.getAllData();
        });

    },
    getAllData: function(){
        var _this = this;
        var requestData = {
            action:"getPointInfo",
            pointId:$('body').data("id")
        }

        $.ajax(_this.get("context_path")+"/point/details",{
            type: 'POST',
            data: requestData,
            success: function (responseData) {
                _this.set("point",responseData.point);

                /*todo: move to view */
                var center = _this.get('point').coord ? eval(_this.get('point').coord) : [59.9190, 30.3529];
                ymaps.ready(function () {
                    var myMap = new ymaps.Map("map", {
                        center: center,
                        zoom: 15,
                        controls: ["zoomControl"]
                    });
                    _this.set("map", myMap);

                    var myPlacemark1 = new ymaps.Placemark(center, {hintContent: _this.get('point').title});

                    /* Clusterize ??
                     var myClusterer = new ymaps.Clusterer();
                     var myGeoObjects = [];

                     myPlacemark1.events.add("click", function (e) {
                     _this.getPointInfo(this.id);
                     }, coord)
                     myGeoObjects[myGeoObjects.length] = myPlacemark1;
                     myClusterer.add(myGeoObjects);
                     */

                    myMap.geoObjects.removeAll();
                    myMap.geoObjects.add(myPlacemark1);
                });
                _this.trigger("render");
            }
        });
    }
});
