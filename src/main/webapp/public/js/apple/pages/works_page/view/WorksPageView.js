var view = Backbone.View.extend({
    initialize: function () {
        var _this = this;

        this.listenTo(this.model, 'render', this.render);
        this.listenTo(this.model, 'renderPointInfo', this.renderPointInfo);
        this.$el = $('#content');

    },
    percentage: function(a,b,c) {
        if(b == 0){
            return 0;
        }else {
            return Math.round(((100 * a) / b) * Math.pow(10, c)) / Math.pow(10, c);
        }
    },
    render: function () {
        var _this = this;
        var point = this.model.get("point");
        var $worksSection = $('#point-works').find('.container.collapse');

        $('[data-target="#point-works-collapse"]').on('click', function () {
            var $shevron = $(this).find('i');
            if ($shevron.hasClass('glyphicon-chevron-up')) {
                $shevron.removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down')
            } else {
                $shevron.removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up')
            }
        })

        var template = $("#years-amount-template").html();

        $('#point-header').html(point.title);
        $('#point-description').html(point.text);
        $('#point-graphics').html(_.template(template, {years: point.years}));

        $pointImages = $('#point-images')
        _.each(point.images, function (image) {
            var $imageHandler = $('<div>', {
                class: 'gallery-preview col-xs-4',
                style: 'background-image: url(' + image + '); background-size: cover; height: 160px;'
            });
            $pointImages.append($('<a>', {
                href: image,
                class: 'gallery',
                rel: 'gallery'
            }).append($imageHandler))
        })

        /* Init Fancybox gallery */
        $('.gallery').fancybox({
            transitionIn: 'elastic',
            transitionOut: 'elastic'
        });

        var dntOptions = {
            segmentShowStroke: true,
            animateRotate: true,
            animateScale: false,
            percentageInnerCutout: 90,
            tooltipFontSize: 10,
            responsive: true,
            showScale: true,
            scaleBeginAtZero: true,
            legendTemplate: "<table class=\"<%=name.toLowerCase()%>Legend\"><% for (var i=0; i<segments.length; i++){%><tr><td><span class=\"legend-pin\" style=\"background-color:<%=segments[i].fillColor%>\"></span></td><td><%if(segments[i].label){%><%=segments[i].label%><%}%>: </td><td style=\"text-align: right;\"><%= numFormat(segments[i].value) %> <i class=\"fa fa-rub\"></i></td></tr><%}%></table>",
            tooltipTemplate: "<%= label %>",
        };

        point.years.forEach(function (year) {
            year.moneyUsed = year.moneyUsed * 1000;
            year.moneyAllocated = year.moneyAllocated * 1000;
            var dntData = [
                {
                    value: year.moneyUsed.toFixed(2),
                    color: "#565c6a",
                    highlight: "#929daf",
                    label: "Использовано"
                },
                {
                    value: Math.round(year.moneyAllocated - year.moneyUsed),
                    color: "#F7464A",
                    highlight: "#F7464A",
                    label: "Недофинансировано"
                },
            ];
            var canvas = document.getElementById("chartDiv-"+year.year);
            var ctx = canvas.getContext("2d");
            var myDntChart = new Chart(ctx).Doughnut(dntData, dntOptions)
            document.getElementById("legendDiv-"+year.year).innerHTML = myDntChart.generateLegend();

            //вставляем значение процента финансирования (анимируем)
            var percent_number_step = $.animateNumber.numberStepFactories.append('%')
            var decimal_places = 2;
            var decimal_factor = decimal_places === 0 ? 1 : Math.pow(10, decimal_places);
            $('#textDiv-' + year.year + ' .animNum').animateNumber({
                number: year.percent * decimal_factor,
                numberStep: function (now, tween) {
                    var floored_number = Math.floor(now) / decimal_factor,
                        target = $(tween.elem);
                    if (decimal_places > 0) {
                        floored_number = floored_number.toFixed(decimal_places);
                    }
                    target.text(floored_number + '%');
                }
            }, 2000);

            //вставляем значение полной суммы (отформатированное)
            document.getElementById("textDiv-"+year.year).getElementsByClassName("sumNum")[0].innerHTML = numFormat(year.moneyAllocated) + ' <i class="fa fa-rub"></i>';

            var $yearRow = $('<div>', {
                class: 'row'
            });

            $yearRow.append($('<h2>').text(year.year + ' год'));

            _.each(year.works, function (work) {
                var $workRow = $('<div>', {
                    class: 'col-xs-11 col-xs-offset-1'
                });

                $workRow.append($('<h4>').text(work.name));
                $workRow.append($('<a>', {
                    class: '',
                    target: '_blank',
                    href: $('body').data('context') + '/page/points/details?id=' + work.id
                }).text('Подробнее'));

                $yearRow.append($workRow)
            })

            $worksSection.append($yearRow)

        });
    }
});
