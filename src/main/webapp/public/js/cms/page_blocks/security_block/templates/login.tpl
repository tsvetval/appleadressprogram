<div class="ml-auth">
    <div class="ml-auth__title">
    </div>
    <div class="ml-auth-blocks-wrap container" style="background-color: #536D9B">
<!--
        <div class="ml-auth-blocks-wrap-inner">
-->
<!--
            <form class="ml-auth__login" action="">
-->
<!--
                <div class="ml-auth__login-bg"></div>
-->
                <div class="ml-auth__login-title row">
                    <div class="col-xs-offset-1 col-xs-23">
                        Авторизация в системе
                    </div>
                </div>
                <div class="ml-auth__login-name row">
                    <input class="ml-auth__login-name-input col-xs-16 col-xs-offset-1" placeholder="Логин" name="login" id="login"/>
                </div>
                <div class="ml-auth__login-password row">
                    <input class="ml-auth__login-password-input col-xs-14 col-xs-offset-1" placeholder="Пароль" type="password" name="pass_login" id="password"/>
                    <span class="ml-auth__login-password-show col-xs-2 glyphicon glyphicon-eye-open" title="Показать пароль"></span>
<!--
                    <div class="ml-auth__login-password-forgot">
                        <a href="#" class="ml-auth__login-password-forgot-link">Забыли?</a>
                    </div>
-->
                </div>
                <div class="row">
                    <div class="col-xs-22 col-xs-offset-1">
                        <button type="submit" class="ml-auth__login-button" id="loginButton">Войти в систему</button>
                    </div>
                </div>
                <div class="ml-auth__login-delimiter"></div>
<!--                <a href="#" class="ml-auth__login-to-token">
                    <span class="ml-auth__login-to-token-bg"></span>
                    <span class="ml-auth__login-to-token-text">Вход по карте или USB-токену</span>
                </a>-->
<!--
            </form>&lt;!&ndash; .ml-auth__login &ndash;&gt;
-->
<!--
            <form class="ml-auth__token" action="">
                <div class="ml-auth__token-bg"></div>
                <div class="ml-auth__token-title">Вход по карте<br />или USB-токену</div>
                <div class="ml-auth__token-password">
                    <input class="ml-auth__token-password-input" placeholder="Пароль" type="password" name="pass_token" />
                    <span class="ml-auth__token-password-show" title="Показать пароль"></span>
                    <div class="ml-auth__token-password-forgot">
                        <a href="#" class="ml-auth__token-password-forgot-link">Забыли?</a>
                    </div>
                </div>
                <button type="submit" class="ml-auth__token-button">Войти в систему</button>
                <div class="ml-auth__token-delimiter"></div>
                <a href="#" class="ml-auth__token-to-login">
                    <span class="ml-auth__token-to-login-bg"></span>
                    <span class="ml-auth__token-to-login-text">Вход по логину и паролю</span>
                </a>
            </form>&lt;!&ndash;.ml-auth__token &ndash;&gt;
-->
<!--
        </div>
-->
    </div>
</div><!-- .ml-auth -->


<!--


<div id="content" style="margin-top: 200px;">
    <div class="col-md-8 col-md-offset-8">
        <div class="centered">
            <h2>Добро пожаловать!
                <div id='test'>
                    <small>тестовые логин и пароль: admin/admin</small>
                </div>
                <div id='error' style="display: none;">
                    <small style="color: red">Ошибка аутентификации!</small>
                </div>
            </h2>
            <div>
                <hr/>
                <div class="row">
                    <label class="col-md-8" style="text-align: left">Имя пользователя</label>

                    <div class="col-md-16">
                        <input class="input form-control" id="login" name="login" value="admin">
                    </div>
                </div>
                <div class="row">
                    <label class="col-md-8" style="text-align: left">Пароль</label>

                    <div class="col-md-16">
                        <input class="input form-control" id="password" name="password" type="password"
                               value="admin">
                    </div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-24">
                        <button id="loginButton" class="col-md-3 btn-primary form-control callback" click-action="loginClick">Войти</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>-->
